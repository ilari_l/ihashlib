#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include "ihashlib.h"

uint64_t tsc()
{
	uint32_t a, b;
	asm volatile("rdtsc" : "=a"(a), "=d"(b));
	return ((uint64_t)b << 32) | a;
}

void bench(const struct hashlib_hash* h1)
{
	uint8_t buffer[65536];
	uint8_t state[4096];
	uint8_t out[256];
	if(h1) {
		h1->init(state);
		h1->update(state, buffer, sizeof(buffer));
		h1->final(state, out);
		uint64_t t1 = tsc();
		for(unsigned i = 0; i < 256; i++) {
			h1->init(state);
			h1->update(state, buffer, sizeof(buffer));
			h1->final(state, out);
		}
		uint64_t t2 = tsc();
		printf("%s: Avg %f clocks/byte\n", h1->name, 1.0 * (t2 - t1) / sizeof(buffer) / 256);
	}
}


int main()
{
	const char** list = hashlib_hash_list();
	while(*list) {
		bench(hashlib_hash_lookup(*list));
		list++;
	}
}

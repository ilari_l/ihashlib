#include "ihashlib.h"
#define DO_CC_INCLUDES
#include "hashes/list.inc"
#undef DO_CC_INCLUDES
#include "utils.h"

#define CANDIDATE(X) do { if(streq(name, NAME_##X )) return & X ; } while(0);
#define ALIAS(X, Y) do { if(streq(name, X )) name = Y ; } while(0);

const struct hashlib_hash* hashlib_hash_lookup(const char* name)
{
#include "hashes/list.inc"
	return NULL;
}

#undef ALIAS
#undef CANDIDATE
#define CANDIDATE(X) NAME_##X ,
#define ALIAS(X, Y) X ,

const char** hashlib_hash_list()
{
	static const char* list[] = {
#include "hashes/list.inc"
		NULL
	};
	return list;
}

const char* hashlib_name(const struct hashlib_hash* hash)
{
	return hash->name;
}

size_t hashlib_block_octets(const struct hashlib_hash* hash)
{
	return hash->block_octets;
}

size_t hashlib_output_octets_min(const struct hashlib_hash* hash)
{
	return hash->output_octets_min;
}

size_t hashlib_output_octets_def(const struct hashlib_hash* hash)
{
	return hash->output_octets_def;
}

size_t hashlib_output_octets_max(const struct hashlib_hash* hash)
{
	return hash->output_octets_max;
}

size_t hashlib_state_size(const struct hashlib_hash* hash)
{
	return hash->state_size;
}

void hashlib_init(const struct hashlib_hash* hash, void* state)
{
	hash->init(state);
}

int hashlib_init_size(const struct hashlib_hash* hash, void* state, size_t size)
{
	if(!hash->init_size) return 0;
	return hash->init_size(state, size);
}

void hashlib_update(const struct hashlib_hash* hash, void* state, const uint8_t* data, size_t datasize)
{
	hash->update(state, data, datasize);
}

void hashlib_final(const struct hashlib_hash* hash, const void* state, uint8_t* hashval)
{
	hash->final(state, hashval);
}

void hashlib_update_v(const struct hashlib_hash* hash, void* state, const struct hashlib_msgf* iov, size_t iovcnt)
{
	if(hash->update_v)
		hash->update_v(state, iov, iovcnt);
	else
		for(size_t i = 0; i < iovcnt; i++)
			hash->update(state, iov[i].msgf_base, iov[i].msgf_len);
}

void hashlib_copy(const struct hashlib_hash* hash, void* dest, const void* src)
{
	hash->copy(dest, src);
}

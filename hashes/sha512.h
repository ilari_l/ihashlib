#ifndef _hashlib_sha512_h_included_
#define _hashlib_sha512_h_included_

#include "ihashlib.h"
#include "libpfx.h"

#define NAME_sha_512 "SHA-512"
#define NAME_sha_384 "SHA-384"
#define NAME_sha_512_256 "SHA-512/256"
#define NAME_sha_512_224 "SHA-512/224"

#define sha_512 FAKE_NOEXPORT(sha_512)
#define sha_384 FAKE_NOEXPORT(sha_384)
#define sha_512_256 FAKE_NOEXPORT(sha_512_256)
#define sha_512_224 FAKE_NOEXPORT(sha_512_224)

extern const struct hashlib_hash sha_512;
extern const struct hashlib_hash sha_384;
extern const struct hashlib_hash sha_512_256;
extern const struct hashlib_hash sha_512_224;

#endif

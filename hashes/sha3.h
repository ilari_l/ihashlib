#ifndef _hashlib_sha3_h_included_
#define _hashlib_sha3_h_included_

#include "ihashlib.h"
#include "libpfx.h"

#define NAME_sha3_512 "SHA3-512"
#define NAME_sha3_384 "SHA3-384"
#define NAME_sha3_256 "SHA3-256"
#define NAME_sha3_224 "SHA3-224"
#define NAME_shake256 "Shake256"
#define NAME_shake128 "Shake128"
#define NAME_shake256_688 "Shake256@688"
#define NAME_shake256_784 "Shake256@784"
#define NAME_shake256_832 "Shake256@832"
#define NAME_shake256_912 "Shake256@912"
#define NAME_shake256_1056 "Shake256@1056"

#define sha3_512 FAKE_NOEXPORT(sha3_512)
#define sha3_384 FAKE_NOEXPORT(sha3_384)
#define sha3_256 FAKE_NOEXPORT(sha3_256)
#define sha3_224 FAKE_NOEXPORT(sha3_224)
#define shake256 FAKE_NOEXPORT(shake256)
#define shake128 FAKE_NOEXPORT(shake128)
#define shake256_688 FAKE_NOEXPORT(shake256_688)
#define shake256_784 FAKE_NOEXPORT(shake256_784)
#define shake256_832 FAKE_NOEXPORT(shake256_832)
#define shake256_912 FAKE_NOEXPORT(shake256_912)
#define shake256_1056 FAKE_NOEXPORT(shake256_1056)

extern const struct hashlib_hash sha3_512;
extern const struct hashlib_hash sha3_384;
extern const struct hashlib_hash sha3_256;
extern const struct hashlib_hash sha3_224;
extern const struct hashlib_hash shake256;
extern const struct hashlib_hash shake128;
extern const struct hashlib_hash shake256_688;
extern const struct hashlib_hash shake256_784;
extern const struct hashlib_hash shake256_832;
extern const struct hashlib_hash shake256_912;
extern const struct hashlib_hash shake256_1056;

#endif

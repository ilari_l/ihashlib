#include "sha3.h"
#include "utils.h"
#define WORDSHIFT 3
#define WORDBYTES (1 << WORDSHIFT)
#define WORDBYTEMASK (WORDBYTES - 1)
typedef uint64_t word_t;

#define ROT(x,p) ((x << (p)) | (x >> ((64 - (p)) & 63)))
#define ROTP(s, i ,p) do { s[i] = ROT(s[i], p); } while(0)

#define THETA_ROW(state, i) (state[0+(i)]^state[5+(i)]^state[10+(i)]^state[15+(i)]^state[20+(i)])
#define THETA_COL(state, i, d) do { state[(i)+0] ^= d; state[(i)+5] ^= d; state[(i)+10] ^= d; state[(i)+15] ^= d; \
	state[(i)+20] ^= d; } while(0)

static void theta(word_t* state)
{
	word_t c0 = THETA_ROW(state, 0);
	word_t c1 = THETA_ROW(state, 1);
	word_t c2 = THETA_ROW(state, 2);
	word_t c3 = THETA_ROW(state, 3);
	word_t c4 = THETA_ROW(state, 4);
	word_t d0 = c4 ^ ROT(c1, 1);
	word_t d1 = c0 ^ ROT(c2, 1);
	word_t d2 = c1 ^ ROT(c3, 1);
	word_t d3 = c2 ^ ROT(c4, 1);
	word_t d4 = c3 ^ ROT(c0, 1);
	THETA_COL(state, 0, d0);
	THETA_COL(state, 1, d1);
	THETA_COL(state, 2, d2);
	THETA_COL(state, 3, d3);
	THETA_COL(state, 4, d4);
}

static void rho(word_t* state)
{
	                     ROTP(state,  1,  1); ROTP(state,  2, 62); ROTP(state,  3, 28); ROTP(state,  4, 27);
	ROTP(state,  5, 36); ROTP(state,  6, 44); ROTP(state,  7,  6); ROTP(state,  8, 55); ROTP(state,  9, 20);
	ROTP(state, 10,  3); ROTP(state, 11, 10); ROTP(state, 12, 43); ROTP(state, 13, 25); ROTP(state, 14, 39);
	ROTP(state, 15, 41); ROTP(state, 16, 45); ROTP(state, 17, 15); ROTP(state, 18, 21); ROTP(state, 19,  8);
	ROTP(state, 20, 18); ROTP(state, 21,  2); ROTP(state, 22, 61); ROTP(state, 23, 56); ROTP(state, 24, 14);
}

static void pi(word_t* state)
{
	word_t tmp = state[1];
	state[ 1] = state[ 6]; state[ 6] = state[ 9]; state[ 9] = state[22]; state[22] = state[14];
	state[14] = state[20]; state[20] = state[ 2]; state[ 2] = state[12]; state[12] = state[13];
	state[13] = state[19]; state[19] = state[23]; state[23] = state[15]; state[15] = state[ 4];
	state[ 4] = state[24]; state[24] = state[21]; state[21] = state[ 8]; state[ 8] = state[16];
	state[16] = state[ 5]; state[ 5] = state[ 3]; state[ 3] = state[18]; state[18] = state[17];
	state[17] = state[11]; state[11] = state[ 7]; state[ 7] = state[10];
	state[10] = tmp;
}

#define CHI_ROW(state, i) do { \
	word_t x0 = state[i + 0]; \
	word_t x1 = state[i + 1]; \
	state[i + 0] ^= (~state[i + 1]) & state[i + 2]; \
	state[i + 1] ^= (~state[i + 2]) & state[i + 3]; \
	state[i + 2] ^= (~state[i + 3]) & state[i + 4]; \
	state[i + 3] ^= (~state[i + 4]) & x0; \
	state[i + 4] ^= (~x0) & x1; \
} while(0)

static void chi(word_t* state)
{
	CHI_ROW(state, 0);
	CHI_ROW(state, 5);
	CHI_ROW(state, 10);
	CHI_ROW(state, 15);
	CHI_ROW(state, 20);
}

static void sha3_compress(word_t* state)
{
	const static word_t rc[24] = {
		0x0000000000000001, 0x0000000000008082, 0x800000000000808a, 0x8000000080008000,
		0x000000000000808b, 0x0000000080000001, 0x8000000080008081, 0x8000000000008009,
		0x000000000000008a, 0x0000000000000088, 0x0000000080008009, 0x000000008000000a,
		0x000000008000808b, 0x800000000000008b, 0x8000000000008089, 0x8000000000008003,
		0x8000000000008002, 0x8000000000000080, 0x000000000000800a, 0x800000008000000a,
		0x8000000080008081, 0x8000000000008080, 0x0000000080000001, 0x8000000080008008,
	};
	for(unsigned round = 0; round < 24; round++) {
		theta(state);
		rho(state);
		pi(state);
		chi(state);
		state[0] ^= rc[round];		//iota.
	}
}

struct sha3_state
{
	word_t state[25];
	word_t maxblock;
	word_t blockfill;
	word_t outbytes;
	word_t fpad;
};

static inline struct sha3_state* unpack_state(void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (struct sha3_state*)((char*)_state + adjust);
}

static inline const struct sha3_state* unpack_state_const(const void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (const struct sha3_state*)((char*)_state + adjust);
}

static void sha3_init_low(void* _state, uint8_t rbytes)
{
	struct sha3_state* state = unpack_state(_state);
	for(unsigned i = 0; i < 25; i++) state->state[i] = 0;
	state->blockfill = 0;
	state->maxblock = 200 - 2 * rbytes;
	state->outbytes = rbytes;
	state->fpad = 6;	//The usual SHA-3.
}

static void sha3_224_init(void* _state) { sha3_init_low(_state, 28); }
static void sha3_256_init(void* _state) { sha3_init_low(_state, 32); }
static void sha3_384_init(void* _state) { sha3_init_low(_state, 48); }
static void sha3_512_init(void* _state) { sha3_init_low(_state, 64); }

static int shake128_init(void* _state, size_t rbytes)
{
	struct sha3_state* state = unpack_state(_state);
	sha3_init_low(_state, 16);
	state->outbytes = rbytes;	//Overwrite the length.
	state->fpad = 31;		//Shake.
	return 1;
}

static int shake256_init(void* _state, size_t rbytes)
{
	struct sha3_state* state = unpack_state(_state);
	sha3_init_low(_state, 32);
	state->outbytes = rbytes;	//Overwrite the length.
	state->fpad = 31;		//Shake.
	return 1;
}

static void shake128d_init(void* _state) { shake128_init(_state, 32); }
static void shake256d_init(void* _state) { shake256_init(_state, 64); }
static void shake256d_688_init(void* _state) { shake256_init(_state, 86); }
static void shake256d_784_init(void* _state) { shake256_init(_state, 98); }
static void shake256d_832_init(void* _state) { shake256_init(_state, 104); }
static void shake256d_912_init(void* _state) { shake256_init(_state, 114); }
static void shake256d_1056_init(void* _state) { shake256_init(_state, 132); }


static inline void _sha3_input(struct sha3_state* state, const uint8_t* buf, size_t bufsize)
{
	while(bufsize > 0) {
		if(state->blockfill == 0 && bufsize >= state->maxblock) {
			//Do entiere block at once.
			xor_load_le64(state->state, buf, state->maxblock >> WORDSHIFT);
			bufsize -= state->maxblock;
			buf += state->maxblock;
			state->blockfill = state->maxblock;
		} else if((state->blockfill & WORDBYTEMASK) == 0 && bufsize >= WORDBYTES) {
			//Do word at once.
			word_t* ptr = &state->state[state->blockfill >> WORDSHIFT];
			xor_load_le64_1(ptr, buf);
			bufsize -= WORDBYTES;
			buf += WORDBYTES;
			state->blockfill += WORDBYTES;
		} else {
			//Do byte at a time.
			unsigned blockfill = state->blockfill;
			state->state[blockfill >> WORDSHIFT] ^= (word_t)buf[0]
				<< ((blockfill & WORDBYTEMASK) << 3);
			bufsize--;
			buf++;
			state->blockfill += 1;
		}
		if(state->blockfill == state->maxblock) {
			//Compress it.
			sha3_compress(state->state);
			state->blockfill = 0;
		}
	}
}

static void sha3_input(void* _state, const uint8_t* buf, size_t bufsize)
{
	struct sha3_state* state = unpack_state(_state);
	_sha3_input(state, buf, bufsize);
}

static void sha3_input_v(void* _state, const struct hashlib_msgf* iov, size_t iovcnt)
{
	struct sha3_state* state = unpack_state(_state);
	for(size_t i = 0; i < iovcnt; i++)
		_sha3_input(state, (const uint8_t*)iov[i].msgf_base, iov[i].msgf_len);
}

static void sha3_result(const void* _state, uint8_t* out)
{
	const struct sha3_state* state = unpack_state_const(_state);
	word_t block[25];
	for(unsigned i = 0; i < 25; i++) block[i] = state->state[i];
	//Pad.
	block[state->blockfill>>WORDSHIFT] ^= state->fpad << ((state->blockfill & WORDBYTEMASK) << 3);
	unsigned last = state->maxblock - 1;
	block[last>>WORDSHIFT] ^= 128ULL << ((last & WORDBYTEMASK) << 3);
	//Start Squeeze
	for(size_t i = 0; i < state->outbytes; i += state->maxblock) {
		sha3_compress(block);
		for(unsigned j = 0; j < state->maxblock && i + j < state->outbytes; j++)
			out[i + j] = block[j >> WORDSHIFT] >> ((j & WORDBYTEMASK) << 3);
	}
}

static void sha3_copy(void* dest, const void* src)
{
	const struct sha3_state* _src = unpack_state_const(src);
	struct sha3_state* _dest = unpack_state(dest);
	*_dest = *_src;
}

const struct hashlib_hash sha3_512 = {
	.name = NAME_sha3_512,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 64, .output_octets_def = 64, .output_octets_max = 64,
	.block_octets = 72,
	.init = sha3_512_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash sha3_384 = {
	.name = NAME_sha3_384,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 48, .output_octets_def = 48, .output_octets_max = 48,
	.block_octets = 104,
	.init = sha3_384_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash sha3_256 = {
	.name = NAME_sha3_256,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 32, .output_octets_def = 32, .output_octets_max = 32,
	.block_octets = 136,
	.init = sha3_256_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash sha3_224 = {
	.name = NAME_sha3_224,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 28, .output_octets_def = 28, .output_octets_max = 28,
	.block_octets = 144,
	.init = sha3_224_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash shake256 = {
	.name = NAME_shake256,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 24, .output_octets_def = 64, .output_octets_max = 2147483647,
	.block_octets = 136,
	.init = shake256d_init,
	.init_size = shake256_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash shake128 = {
	.name = NAME_shake128,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 24, .output_octets_def = 32, .output_octets_max = 2147483647,
	.block_octets = 168,
	.init = shake128d_init,
	.init_size = shake128_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash shake256_688 = {
	.name = NAME_shake256_688,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 86, .output_octets_def = 86, .output_octets_max = 86,
	.block_octets = 136,
	.init = shake256d_688_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash shake256_784 = {
	.name = NAME_shake256_784,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 98, .output_octets_def = 98, .output_octets_max = 98,
	.block_octets = 136,
	.init = shake256d_784_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash shake256_832 = {
	.name = NAME_shake256_832,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 104, .output_octets_def = 104, .output_octets_max = 104,
	.block_octets = 136,
	.init = shake256d_832_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash shake256_912 = {
	.name = NAME_shake256_912,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 114, .output_octets_def = 114, .output_octets_max = 114,
	.block_octets = 136,
	.init = shake256d_912_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

const struct hashlib_hash shake256_1056 = {
	.name = NAME_shake256_1056,
	.state_size = sizeof(struct sha3_state) + sizeof(word_t),
	.output_octets_min = 132, .output_octets_def = 132, .output_octets_max = 132,
	.block_octets = 136,
	.init = shake256d_1056_init,
	.update = sha3_input,
	.final = sha3_result,
	.update_v = sha3_input_v,
	.copy = sha3_copy,
};

#ifndef _hashlib_sha256_h_included_
#define _hashlib_sha256_h_included_

#include "ihashlib.h"
#include "libpfx.h"

#define NAME_sha_256 "SHA-256"
#define NAME_sha_224 "SHA-224"

#define sha_256 FAKE_NOEXPORT(sha_256)
#define sha_224 FAKE_NOEXPORT(sha_224)

extern const struct hashlib_hash sha_224;
extern const struct hashlib_hash sha_256;

#endif

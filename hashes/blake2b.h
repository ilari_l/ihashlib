#ifndef _hashlib_blake2b_h_included_
#define _hashlib_blake2b_h_included_

#include "ihashlib.h"
#include "libpfx.h"

#define NAME_blake2b_512 "Blake2b-512"
#define NAME_blake2b_384 "Blake2b-384"
#define NAME_blake2b_256 "Blake2b-256"
#define NAME_blake2b_224 "Blake2b-224"

#define blake2b_512 FAKE_NOEXPORT(blake2b_512)
#define blake2b_384 FAKE_NOEXPORT(blake2b_384)
#define blake2b_256 FAKE_NOEXPORT(blake2b_256)
#define blake2b_224 FAKE_NOEXPORT(blake2b_224)

extern const struct hashlib_hash blake2b_512;
extern const struct hashlib_hash blake2b_384;
extern const struct hashlib_hash blake2b_256;
extern const struct hashlib_hash blake2b_224;

#endif

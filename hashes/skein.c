#include "skein.h"
#include "utils.h"
#include <stdio.h>
#define WORDSHIFT 3
#define WORDBYTES (1 << WORDSHIFT)
#define WORDBYTEMASK (WORDBYTES - 1)
typedef uint64_t word_t;

#define RX(A, B, R) do { A += B; B = (B << (R)) | (B >> ((64 - (R)) & 63)); B ^= A; } while(0)

/*
static void show_array(const char* prefix, const uint64_t* a, size_t e)
{
	fprintf(stderr, "%s", prefix);
	for(size_t i = 0; i < e; i++) {
		fprintf(stderr, "%016lx", a[i]);
		if(i < e - 1)
			fprintf(stderr, ", ");
	}
	fprintf(stderr, "\n");
}
*/
//**************************** Skein256 *************************
const static unsigned rt256_1[] = {14, 16, 52, 57, 23, 40,  5, 37};
const static unsigned rt256_2[] = {25, 33, 46, 12, 58, 22, 32, 32};

#define SKEIN256_4(X, R) do { \
	RX(X[0], X[1], R[0]); RX(X[2], X[3], R[1]); \
	RX(X[0], X[3], R[2]); RX(X[2], X[1], R[3]); \
	RX(X[0], X[1], R[4]); RX(X[2], X[3], R[5]); \
	RX(X[0], X[3], R[6]); RX(X[2], X[1], R[7]); \
} while(0)

#define SKEIN256_MK(X, K, T, N) do { \
	X[0] += K[(N + 0)%5];                X[1] += K[(N + 1)%5] + T[(N + 0)%3]; \
	X[2] += K[(N + 2)%5] + T[(N + 1)%3]; X[3] += K[(N + 3)%5] + N; \
} while(0)

#define SKEIN256_8(R, M, K, T) do { \
	SKEIN256_4(M, rt256_1); SKEIN256_MK(M, K, T, 2 * (R) + 1); \
	SKEIN256_4(M, rt256_2); SKEIN256_MK(M, K, T, 2 * (R) + 2); \
	} while(0)

#define SKEIN256(M, K, T) do { \
	SKEIN256_MK(M, K, T, 0); \
	SKEIN256_8(0, M, K, T); \
	SKEIN256_8(1, M, K, T); \
	SKEIN256_8(2, M, K, T); \
	SKEIN256_8(3, M, K, T); \
	SKEIN256_8(4, M, K, T); \
	SKEIN256_8(5, M, K, T); \
	SKEIN256_8(6, M, K, T); \
	SKEIN256_8(7, M, K, T); \
	SKEIN256_8(8, M, K, T); \
} while(0)

#define WORDS 4
static void skein256_compress(word_t* chain, const word_t* block, word_t t0, word_t t1)
{
	word_t key[WORDS+1];
	word_t tweak[3] = {t0, t1, t0 ^ t1};
	key[WORDS] = 0x1BD11BDAA9FC1A22ULL;
	for(unsigned i = 0; i < WORDS; i++) {
		key[i] = chain[i];
		key[WORDS] ^= chain[i];
		chain[i] = block[i];
	}
	SKEIN256(chain, key, tweak);
	for(unsigned i = 0; i < WORDS; i++) chain[i] ^= block[i];
}
#undef WORDS

//**************************** Skein512 *************************
const static unsigned rt512_1[] = {46, 36, 19, 37, 33, 27, 14, 42, 17, 49, 36, 39, 44,  9, 54, 56};
const static unsigned rt512_2[] = {39, 30, 34, 24, 13, 50, 10, 17, 25, 29, 39, 43,  8, 35, 56, 22};

#define SKEIN512_4(X, R) do { \
	RX(X[0], X[1], R[ 0]); RX(X[2], X[3], R[ 1]); RX(X[4], X[5], R[ 2]); RX(X[6], X[7], R[ 3]); \
	RX(X[2], X[1], R[ 4]); RX(X[4], X[7], R[ 5]); RX(X[6], X[5], R[ 6]); RX(X[0], X[3], R[ 7]); \
	RX(X[4], X[1], R[ 8]); RX(X[6], X[3], R[ 9]); RX(X[0], X[5], R[10]); RX(X[2], X[7], R[11]); \
	RX(X[6], X[1], R[12]); RX(X[0], X[7], R[13]); RX(X[2], X[5], R[14]); RX(X[4], X[3], R[15]); \
} while(0)

#define SKEIN512_MK(X, K, T, N) do { \
	X[0] += K[(N + 0)%9];                X[1] += K[(N + 1)%9]; \
	X[2] += K[(N + 2)%9];                X[3] += K[(N + 3)%9]; \
	X[4] += K[(N + 4)%9];                X[5] += K[(N + 5)%9] + T[(N + 0)%3]; \
	X[6] += K[(N + 6)%9] + T[(N + 1)%3]; X[7] += K[(N + 7)%9] + N; \
} while(0)

#define SKEIN512_8(R, M, K, T) do { \
	SKEIN512_4(M, rt512_1); SKEIN512_MK(M, K, T, 2 * (R) + 1); \
	SKEIN512_4(M, rt512_2); SKEIN512_MK(M, K, T, 2 * (R) + 2); \
} while(0)

#define SKEIN512(M, K, T) do { \
	SKEIN512_MK(M, K, T, 0); \
	SKEIN512_8(0, M, K, T); \
	SKEIN512_8(1, M, K, T); \
	SKEIN512_8(2, M, K, T); \
	SKEIN512_8(3, M, K, T); \
	SKEIN512_8(4, M, K, T); \
	SKEIN512_8(5, M, K, T); \
	SKEIN512_8(6, M, K, T); \
	SKEIN512_8(7, M, K, T); \
	SKEIN512_8(8, M, K, T); \
} while(0)

#define WORDS 8
static void skein512_compress(word_t* chain, const word_t* block, word_t t0, word_t t1)
{
	word_t key[WORDS+1];
	word_t tweak[3] = {t0, t1, t0 ^ t1};
	key[WORDS] = 0x1BD11BDAA9FC1A22ULL;
	for(unsigned i = 0; i < WORDS; i++) {
		key[i] = chain[i];
		key[WORDS] ^= chain[i];
		chain[i] = block[i];
	}
	SKEIN512(chain, key, tweak);
	for(unsigned i = 0; i < WORDS; i++) chain[i] ^= block[i];
}
#undef WORDS

//**************************** Skein1024 *************************
const static unsigned rt1024_1[] = {24, 13,  8, 47,  8, 17, 22, 37, 38, 19, 10, 55, 49, 18, 23, 52, 
				    33,  4, 51, 13, 34, 41, 59, 17,  5, 20, 48, 41, 47, 28, 16, 25};
const static unsigned rt1024_2[] = {41,  9, 37, 31, 12, 47, 44, 30, 16, 34, 56, 51,  4, 53, 42, 41,
				    31, 44, 47, 46, 19, 42, 44, 25,  9, 48, 35, 52, 23, 31, 37, 20};

#define SKEIN1024_4(X, R) do { \
	RX(X[ 0], X[ 1], R[ 0]); RX(X[ 2], X[ 3], R[ 1]); RX(X[ 4], X[ 5], R[ 2]);  RX(X[ 6], X[ 7], R[ 3]); \
	RX(X[ 8], X[ 9], R[ 4]); RX(X[10], X[11], R[ 5]); RX(X[12], X[13], R[ 6]);  RX(X[14], X[15], R[ 7]); \
	RX(X[ 0], X[ 9], R[ 8]); RX(X[ 2], X[13], R[ 9]); RX(X[ 6], X[11], R[10]);  RX(X[ 4], X[15], R[11]); \
	RX(X[10], X[ 7], R[12]); RX(X[12], X[ 3], R[13]); RX(X[14], X[ 5], R[14]);  RX(X[ 8], X[ 1], R[15]); \
	RX(X[ 0], X[ 7], R[16]); RX(X[ 2], X[ 5], R[17]); RX(X[ 4], X[ 3], R[18]);  RX(X[ 6], X[ 1], R[19]); \
	RX(X[12], X[15], R[20]); RX(X[14], X[13], R[21]); RX(X[ 8], X[11], R[22]);  RX(X[10], X[ 9], R[23]); \
	RX(X[ 0], X[15], R[24]); RX(X[ 2], X[11], R[25]); RX(X[ 6], X[13], R[26]);  RX(X[ 4], X[ 9], R[27]); \
	RX(X[14], X[ 1], R[28]); RX(X[ 8], X[ 5], R[29]); RX(X[10], X[ 3], R[30]);  RX(X[12], X[ 7], R[31]); \
} while(0)

#define SKEIN1024_MK(X, K, T, N) do { \
	X[0] += K[(N + 0)%17];                  X[1] += K[(N + 1)%17]; \
	X[2] += K[(N + 2)%17];                  X[3] += K[(N + 3)%17]; \
	X[4] += K[(N + 4)%17];                  X[5] += K[(N + 5)%17]; \
	X[6] += K[(N + 6)%17];                  X[7] += K[(N + 7)%17]; \
	X[8] += K[(N + 8)%17];                  X[9] += K[(N + 9)%17]; \
	X[10] += K[(N + 10)%17];                X[11] += K[(N + 11)%17]; \
	X[12] += K[(N + 12)%17];                X[13] += K[(N + 13)%17] + T[(N + 0)%3]; \
	X[14] += K[(N + 14)%17] + T[(N + 1)%3]; X[15] += K[(N + 15)%17] + N; \
} while(0)

#define SKEIN1024_8(R, M, K, T) do { \
	SKEIN1024_4(M, rt1024_1); SKEIN1024_MK(M, K, T, 2 * (R) + 1); \
	SKEIN1024_4(M, rt1024_2); SKEIN1024_MK(M, K, T, 2 * (R) + 2); \
} while(0)

#define SKEIN1024(M, K, T) do { \
	SKEIN1024_MK(M, K, T, 0); \
	SKEIN1024_8(0, M, K, T); \
	SKEIN1024_8(1, M, K, T); \
	SKEIN1024_8(2, M, K, T); \
	SKEIN1024_8(3, M, K, T); \
	SKEIN1024_8(4, M, K, T); \
	SKEIN1024_8(5, M, K, T); \
	SKEIN1024_8(6, M, K, T); \
	SKEIN1024_8(7, M, K, T); \
	SKEIN1024_8(8, M, K, T); \
	SKEIN1024_8(9, M, K, T); \
} while(0)

#define WORDS 16
static void skein1024_compress(word_t* chain, const word_t* block, word_t t0, word_t t1)
{
	word_t key[WORDS+1];
	word_t tweak[3] = {t0, t1, t0 ^ t1};
	key[WORDS] = 0x1BD11BDAA9FC1A22ULL;
	for(unsigned i = 0; i < WORDS; i++) {
		key[i] = chain[i];
		key[WORDS] ^= chain[i];
		chain[i] = block[i];
	}
	SKEIN1024(chain, key, tweak);
	for(unsigned i = 0; i < WORDS; i++) chain[i] ^= block[i];
}
#undef WORDS


//****************************************************************
struct skein_state
{
	word_t chain[16];
	word_t block[16];
	word_t maxblock;
	word_t blockmask;
	word_t count_lo;
	word_t count_hi;
	word_t outbytes;
	void (*compress)(word_t* chain, const word_t* block, word_t t0, word_t t1);
	int started;
};

static inline struct skein_state* unpack_state(void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (struct skein_state*)((char*)_state + adjust);
}

static inline const struct skein_state* unpack_state_const(const void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (const struct skein_state*)((char*)_state + adjust);
}

static int skein_init_common(struct skein_state* state, size_t rbytes)
{
	for(unsigned i = 0; i < 16; i++) state->chain[i] = 0;
	for(unsigned i = 0; i < 16; i++) state->block[i] = 0;
	state->count_hi = 0;
	state->count_lo = 0;
	state->blockmask = state->maxblock - 1;
	state->outbytes = rbytes;
	state->started = 0;
	word_t config[16] = {0};
	config[0] = 0x133414853;
	config[1] = 8 * rbytes;
	state->compress(state->chain, config, 32, 0xC4ULL << 56);
	return 1;
}

static int skein256_init(void* _state, size_t rbytes)
{
	struct skein_state* state = unpack_state(_state);
	state->maxblock = 32;
	state->compress = skein256_compress;
	return skein_init_common(state, rbytes);
}

static int skein512_init(void* _state, size_t rbytes)
{
	struct skein_state* state = unpack_state(_state);
	state->maxblock = 64;
	state->compress = skein512_compress;
	return skein_init_common(state, rbytes);
}

static int skein1024_init(void* _state, size_t rbytes)
{
	struct skein_state* state = unpack_state(_state);
	state->maxblock = 128;
	state->compress = skein1024_compress;
	return skein_init_common(state, rbytes);
}

static void skein256d_init(void* _state) { skein256_init(_state, 32); }
static void skein512d_init(void* _state) { skein256_init(_state, 64); }
static void skein1024d_init(void* _state) { skein256_init(_state, 128); }

static inline void _skein_input(struct skein_state* state, const uint8_t* buf, size_t bufsize)
{
	while(bufsize > 0) {
		if((state->count_lo & state->blockmask) == 0 && (state->count_lo | state->count_hi) > 0) {
			//Compress it.
			uint64_t t1 = 0x30;
			if(!state->started) t1 |= 0x40;
			t1 = state->count_hi | (t1 << 56);
			state->compress(state->chain, state->block, state->count_lo, t1);
			for(unsigned i = 0; i < 16; i++) state->block[i] = 0;
			state->started = 1;
		}
		if((state->count_lo & state->blockmask) == 0 && bufsize >= state->maxblock) {
			//Do entiere block at once.
			xor_load_le64(state->block, buf, state->maxblock >> WORDSHIFT);
			bufsize -= state->maxblock;
			buf += state->maxblock;
			state->count_lo += state->maxblock;
		} else if((state->count_lo & WORDBYTEMASK) == 0 && bufsize >= WORDBYTES) {
			//Do word at once.
			word_t* ptr = &state->block[(state->count_lo & state->blockmask) >> WORDSHIFT];
			xor_load_le64_1(ptr, buf);
			bufsize -= WORDBYTES;
			buf += WORDBYTES;
			state->count_lo += WORDBYTES;
		} else {
			//Do byte at a time.
			unsigned blockfill = (state->count_lo & state->blockmask);
			state->block[blockfill >> WORDSHIFT] |= (word_t)buf[0]
				<< ((blockfill & WORDBYTEMASK) << 3);
			bufsize--;
			buf++;
			state->count_lo += 1;
		}
		//Any multiple increment has to be algined, so this can't overflow past zero.
		if(!state->count_lo) state->count_hi++;
	}
}

static void skein_input(void* _state, const uint8_t* buf, size_t bufsize)
{
	struct skein_state* state = unpack_state(_state);
	_skein_input(state, buf, bufsize);
}

static void skein_input_v(void* _state, const struct hashlib_msgf* iov, size_t iovcnt)
{
	struct skein_state* state = unpack_state(_state);
	for(size_t i = 0; i < iovcnt; i++)
		_skein_input(state, (const uint8_t*)iov[i].msgf_base, iov[i].msgf_len);
}

static void skein_result(const void* _state, uint8_t* out)
{
	const struct skein_state* state = unpack_state_const(_state);
	word_t chain[16];
	word_t tmp[16];
	word_t block[16];
	//Finish the output.
	uint64_t t1 = 0xB0;
	if(!state->started) t1 |= 0x40;
	t1 = state->count_hi | (t1 << 56);
	for(unsigned i = 0; i < 16; i++) chain[i] = state->chain[i];
	state->compress(chain, state->block, state->count_lo, t1);
	
	for(unsigned i = 0; i < 16; i++) block[i] = 0;
	for(size_t ooffset = 0; ooffset < state->outbytes; ooffset += state->maxblock) {
		for(unsigned i = 0; i < 16; i++) tmp[i] = chain[i];
		state->compress(tmp, block, 8, 0xFFULL << 56);
		block[0]++;
		for(unsigned i = 0; i < state->maxblock && ooffset + i < state->outbytes; i++)
			out[ooffset + i] = tmp[i >> WORDSHIFT] >> ((i & WORDBYTEMASK) << 3);
	}
}

static void skein_copy(void* dest, const void* src)
{
	const struct skein_state* _src = unpack_state_const(src);
	struct skein_state* _dest = unpack_state(dest);
	*_dest = *_src;
}

const struct hashlib_hash skein256 = {
	.name = NAME_skein256,
	.state_size = sizeof(struct skein_state) + sizeof(word_t),
	.output_octets_min = 24, .output_octets_def = 32, .output_octets_max = 2147483647,
	.block_octets = 32,
	.init = skein256d_init,
	.init_size = skein256_init,
	.update = skein_input,
	.final = skein_result,
	.update_v = skein_input_v,
	.copy = skein_copy,
};

const struct hashlib_hash skein512 = {
	.name = NAME_skein512,
	.state_size = sizeof(struct skein_state) + sizeof(word_t),
	.output_octets_min = 24, .output_octets_def = 64, .output_octets_max = 2147483647,
	.block_octets = 64,
	.init = skein512d_init,
	.init_size = skein512_init,
	.update = skein_input,
	.final = skein_result,
	.update_v = skein_input_v,
	.copy = skein_copy,
};

const struct hashlib_hash skein1024 = {
	.name = NAME_skein1024,
	.state_size = sizeof(struct skein_state) + sizeof(word_t),
	.output_octets_min = 24, .output_octets_def = 128, .output_octets_max = 2147483647,
	.block_octets = 128,
	.init = skein1024d_init,
	.init_size = skein1024_init,
	.update = skein_input,
	.final = skein_result,
	.update_v = skein_input_v,
	.copy = skein_copy,
};

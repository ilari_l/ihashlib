#ifndef _hashlib_skein_h_included_
#define _hashlib_skein_h_included_

#include "ihashlib.h"
#include "libpfx.h"

#define NAME_skein256 "Skein-256"
#define NAME_skein512 "Skein-512"
#define NAME_skein1024 "Skein-1024"

#define skein256 FAKE_NOEXPORT(skein256)
#define skein512 FAKE_NOEXPORT(skein512)
#define skein1024 FAKE_NOEXPORT(skein1024)

extern const struct hashlib_hash skein256;
extern const struct hashlib_hash skein512;
extern const struct hashlib_hash skein1024;

#endif

#include "sha256.h"

typedef uint32_t word_t;
#define SIGMA0_TAP1 2
#define SIGMA0_TAP2 13
#define SIGMA0_TAP3 22
#define SIGMA1_TAP1 6
#define SIGMA1_TAP2 11
#define SIGMA1_TAP3 25
#define ESIGMA0_TAP1 7
#define ESIGMA0_TAP2 18
#define ESIGMA0_SHIFT 3
#define ESIGMA1_TAP1 17
#define ESIGMA1_TAP2 19
#define ESIGMA1_SHIFT 10
#define MAJ_ROUNDS 4
#define BLOCKBYTES 64
#define BLOCKBITS (8*BLOCKBYTES)
#define BLOCKBITMASK (BLOCKBITS-1)
#define WORDSHIFT 2
#define WORDBYTES (1<<WORDSHIFT)
#define WORDHBYTE (WORDBYTES-1)
#define WORDBITS (8*WORDBYTES)
#define WORDBITMASK (WORDBITS-1)
#define XORLOAD xor_load_be32
#define XORLOAD_1 xor_load_be32_1

static const word_t sha256_iv[8] = {
	0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a, 0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19
};

static const word_t sha224_iv[8] = {
	0xc1059ed8, 0x367cd507, 0x3070dd17, 0xf70e5939, 0xffc00b31, 0x68581511, 0x64f98fa7, 0xbefa4fa4
};

//The SHA-256 round constants.
static const word_t sha2_rk[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
};

#include "sha2.inc"

static void sha224_init(void* _state) { sha2_init_low(_state, sha224_iv, 28); }
static void sha256_init(void* _state) { sha2_init_low(_state, sha256_iv, 32); }

const struct hashlib_hash sha_256 = {
	.name = NAME_sha_256,
	.state_size = sizeof(struct sha2_state) + sizeof(word_t),
	.output_octets_min = 32, .output_octets_def = 32, .output_octets_max = 32,
	.block_octets = BLOCKBYTES,
	.init = sha256_init,
	.update = sha2_input,
	.final = sha2_result,
	.update_v = sha2_input_v,
	.copy = sha2_copy,
};

const struct hashlib_hash sha_224 = {
	.name = NAME_sha_224,
	.state_size = sizeof(struct sha2_state) + sizeof(word_t),
	.output_octets_min = 28, .output_octets_def = 28, .output_octets_max = 28,
	.block_octets = BLOCKBYTES,
	.init = sha224_init,
	.update = sha2_input,
	.final = sha2_result,
	.update_v = sha2_input_v,
	.copy = sha2_copy,
};

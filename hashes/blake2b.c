#ifndef BLAKE2B_REFERENCE
#include "blake2b.h"
#include "utils.h"

#define BLOCKBYTES 128
#define BLOCKBYTEMASK (BLOCKBYTES - 1)
#define WORDSHIFT 3
#define WORDBYTES (1 << WORDSHIFT)
#define WORDBYTEMASK (WORDBYTES - 1)

#define ALGOID_blake2b_224 FAKE_NOEXPORT(ALGOID_blake2b_224)
#define ALGOID_blake2b_256 FAKE_NOEXPORT(ALGOID_blake2b_256)
#define ALGOID_blake2b_384 FAKE_NOEXPORT(ALGOID_blake2b_384)
#define ALGOID_blake2b_512 FAKE_NOEXPORT(ALGOID_blake2b_512)

typedef uint64_t word_t;

static const word_t blake2b_iv[8] = {
	0x6A09E667F3BCC908, 0xBB67AE8584CAA73B, 0x3C6EF372FE94F82B, 0xA54FF53A5F1D36F1,
	0x510E527FADE682D1, 0x9B05688C2B3E6C1F, 0x1F83D9ABFB41BD6B, 0x5BE0CD19137E2179
};

const uint8_t ALGOID_blake2b_224[17] = {
	0x30, 0x0f, 0x06, 0x0b, 0x2b, 0x06, 0x01, 0x04, 0x01, 0x8d, 0x3a, 0x0c, 0x02, 0x01, 0x1C, 0x05, 0x00
};

const uint8_t ALGOID_blake2b_256[17] = {
	0x30, 0x0f, 0x06, 0x0b, 0x2b, 0x06, 0x01, 0x04, 0x01, 0x8d, 0x3a, 0x0c, 0x02, 0x01, 0x20, 0x05, 0x00
};

const uint8_t ALGOID_blake2b_384[17] = {
	0x30, 0x0f, 0x06, 0x0b, 0x2b, 0x06, 0x01, 0x04, 0x01, 0x8d, 0x3a, 0x0c, 0x02, 0x01, 0x30, 0x05, 0x00
};

const uint8_t ALGOID_blake2b_512[17] = {
	0x30, 0x0f, 0x06, 0x0b, 0x2b, 0x06, 0x01, 0x04, 0x01, 0x8d, 0x3a, 0x0c, 0x02, 0x01, 0x40, 0x05, 0x00
};


struct blake2b_state
{
	word_t chain[8];
	word_t block[16];
	word_t count_lo;
	word_t count_hi;
	word_t outbytes;
};

static inline struct blake2b_state* unpack_state(void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (struct blake2b_state*)((char*)_state + adjust);
}

static inline const struct blake2b_state* unpack_state_const(const void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (const struct blake2b_state*)((char*)_state + adjust);
}

static word_t rotate(word_t x, unsigned a) { return (x >> a) | (x << (64 - a)); }

#define SUBROUND(W, A, B, C, D, X, Y) do { \
	W[A] = W[A] + W[B] + X; \
	W[D] = rotate(W[D] ^ W[A], 32); \
	W[C] = W[C] + W[D]; \
	W[B] = rotate(W[B] ^ W[C], 24); \
	W[A] = W[A] + W[B] + Y; \
	W[D] = rotate(W[D] ^ W[A], 16); \
	W[C] = W[C] + W[D]; \
	W[B] = rotate(W[B] ^ W[C], 63); \
	} while(0)

#define ROUND(W, M, I0, I1, I2, I3, I4, I5, I6, I7, I8, I9, I10, I11, I12, I13, I14, I15) do { \
	SUBROUND(W, 0, 4,  8, 12, M[I0], M[I1]); \
	SUBROUND(W, 1, 5,  9, 13, M[I2], M[I3]); \
	SUBROUND(W, 2, 6, 10, 14, M[I4], M[I5]); \
	SUBROUND(W, 3, 7, 11, 15, M[I6], M[I7]); \
	SUBROUND(W, 0, 5, 10, 15, M[I8], M[I9]); \
	SUBROUND(W, 1, 6, 11, 12, M[I10], M[I11]); \
	SUBROUND(W, 2, 7,  8, 13, M[I12], M[I13]); \
	SUBROUND(W, 3, 4,  9, 14, M[I14], M[I15]); \
	} while(0)
	

static void blake2b_compress(word_t* chain, word_t* block, word_t lo, word_t hi, int final)
{
	word_t work[16];
	for(unsigned i = 0; i < 8; i++) work[i] = chain[i];
	for(unsigned i = 0; i < 8; i++) work[i+8] = blake2b_iv[i];
	work[12] ^= lo;
	work[13] ^= hi;
	if(final) work[14] ^= -(word_t)1;
	ROUND(work, block,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15);
	ROUND(work, block, 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3);
	ROUND(work, block, 11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4);
	ROUND(work, block,  7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8);
	ROUND(work, block,  9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13);
	ROUND(work, block,  2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9);
	ROUND(work, block, 12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11);
	ROUND(work, block, 13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10);
	ROUND(work, block,  6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5);
	ROUND(work, block, 10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13,  0);
	ROUND(work, block,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15);
	ROUND(work, block, 14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3);
	for(unsigned i = 0; i < 8; i++) chain[i] ^= work[i] ^ work[i+8];
}

static void blake2b_init_low(void* _state, const word_t iv[8], uint8_t rbytes)
{
	struct blake2b_state* state = unpack_state(_state);
	for(unsigned i = 0; i < 8; i++) state->chain[i] = iv[i];
	state->chain[0] ^= 0x01010000 | rbytes;
	for(unsigned i = 0; i < 16; i++) state->block[i] = 0;
	state->count_lo = 0;
	state->count_hi = 0;
	state->outbytes = rbytes;
}

static void blake2b_224_init(void* _state) { blake2b_init_low(_state, blake2b_iv, 28); }
static void blake2b_256_init(void* _state) { blake2b_init_low(_state, blake2b_iv, 32); }
static void blake2b_384_init(void* _state) { blake2b_init_low(_state, blake2b_iv, 48); }
static void blake2b_512_init(void* _state) { blake2b_init_low(_state, blake2b_iv, 64); }

static inline void _blake2b_input(struct blake2b_state* state, const uint8_t* buf, size_t bufsize)
{
	while(bufsize > 0) {
		if((state->count_lo & BLOCKBYTEMASK) == 0 && (state->count_lo | state->count_hi) > 0) {
			//Compress it.
			blake2b_compress(state->chain, state->block, state->count_lo, state->count_hi, 0);
			for(unsigned i = 0; i < 16; i++) state->block[i] = 0;
		}
		if((state->count_lo & BLOCKBYTEMASK) == 0 && bufsize >= BLOCKBYTES) {
			//Do entiere block at once.
			xor_load_le64(state->block, buf, BLOCKBYTES >> WORDSHIFT);
			bufsize -= BLOCKBYTES;
			buf += BLOCKBYTES;
			state->count_lo += BLOCKBYTES;
		} else if((state->count_lo & WORDBYTEMASK) == 0 && bufsize >= WORDBYTES) {
			//Do word at once.
			word_t* ptr = &state->block[(state->count_lo & BLOCKBYTEMASK) >> WORDSHIFT];
			xor_load_le64_1(ptr, buf);
			bufsize -= WORDBYTES;
			buf += WORDBYTES;
			state->count_lo += WORDBYTES;
		} else {
			//Do byte at a time.
			unsigned blockfill = (state->count_lo & BLOCKBYTEMASK);
			state->block[blockfill >> WORDSHIFT] |= (word_t)buf[0]
				<< ((blockfill & WORDBYTEMASK) << 3);
			bufsize--;
			buf++;
			state->count_lo += 1;
		}
		//Any multiple increment has to be algined, so this can't overflow past zero.
		if(!state->count_lo) state->count_hi++;
	}
}

static void blake2b_input(void* _state, const uint8_t* buf, size_t bufsize)
{
	struct blake2b_state* state = unpack_state(_state);
	_blake2b_input(state, buf, bufsize);
}

static void blake2b_input_v(void* _state, const struct hashlib_msgf* iov, size_t iovcnt)
{
	struct blake2b_state* state = unpack_state(_state);
	for(size_t i = 0; i < iovcnt; i++)
		_blake2b_input(state, (const uint8_t*)iov[i].msgf_base, iov[i].msgf_len);
}

static void blake2b_result(const void* _state, uint8_t* out)
{
	const struct blake2b_state* state = unpack_state_const(_state);
	word_t chain[8];
	word_t block[16];
	word_t count_lo = state->count_lo;
	word_t count_hi = state->count_hi;
	unsigned bytes = state->outbytes;
	for(unsigned i = 0; i < 8; i++) chain[i] = state->chain[i];
	for(unsigned i = 0; i < 16; i++) block[i] = state->block[i];
	blake2b_compress(chain, block, count_lo, count_hi, 1);
	//Unpack the chain.
	for(unsigned i = 0; i < bytes; i++)
		out[i] = chain[i >> WORDSHIFT] >> ((i & WORDBYTEMASK) << 3);
}

static void blake2b_copy(void* dest, const void* src)
{
	const struct blake2b_state* _src = unpack_state_const(src);
	struct blake2b_state* _dest = unpack_state(dest);
	*_dest = *_src;
}

const struct hashlib_hash blake2b_512 = {
	.name = NAME_blake2b_512,
	.state_size = sizeof(struct blake2b_state) + sizeof(word_t),
	.output_octets_min = 64, .output_octets_def = 64, .output_octets_max = 64,
	.block_octets = BLOCKBYTES,
	.init = blake2b_512_init,
	.update = blake2b_input,
	.final = blake2b_result,
	.update_v = blake2b_input_v,
	.copy = blake2b_copy,
};

const struct hashlib_hash blake2b_384 = {
	.name = NAME_blake2b_384,
	.state_size = sizeof(struct blake2b_state) + sizeof(word_t),
	.output_octets_min = 48, .output_octets_def = 48, .output_octets_max = 48,
	.block_octets = BLOCKBYTES,
	.init = blake2b_384_init,
	.update = blake2b_input,
	.final = blake2b_result,
	.update_v = blake2b_input_v,
	.copy = blake2b_copy,
};

const struct hashlib_hash blake2b_256 = {
	.name = NAME_blake2b_256,
	.state_size = sizeof(struct blake2b_state) + sizeof(word_t),
	.output_octets_min = 32, .output_octets_def = 32, .output_octets_max = 32,
	.block_octets = BLOCKBYTES,
	.init = blake2b_256_init,
	.update = blake2b_input,
	.final = blake2b_result,
	.update_v = blake2b_input_v,
	.copy = blake2b_copy,
};

const struct hashlib_hash blake2b_224 = {
	.name = NAME_blake2b_224,
	.state_size = sizeof(struct blake2b_state) + sizeof(word_t),
	.output_octets_min = 28, .output_octets_def = 28, .output_octets_max = 68,
	.block_octets = BLOCKBYTES,
	.init = blake2b_224_init,
	.update = blake2b_input,
	.final = blake2b_result,
	.update_v = blake2b_input_v,
	.copy = blake2b_copy,
};
#endif

struct sha2_state
{
	word_t chain[8];
	word_t block[16];
	word_t bits_lo;
	word_t bits_hi;
	word_t outsize;
};

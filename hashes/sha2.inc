#include "sha2state.inc"
#include "utils.h"

static inline word_t rotate_r(word_t num, unsigned places)
{
	return (num >> places) | (num << (8 * sizeof(word_t) - places));
}

static inline word_t majority(word_t a, word_t b, word_t c)
{
	return ((a & b) ^ (a & c) ^ (b & c));
}

static inline word_t choose(word_t k, word_t a, word_t b)
{
	return (k & a) | ((~k) & b);
}

static inline word_t sigma0(word_t num)
{
	return rotate_r(num, SIGMA0_TAP1) ^ rotate_r(num, SIGMA0_TAP2) ^ rotate_r(num, SIGMA0_TAP3);
}

static inline word_t sigma1(word_t num)
{
	return rotate_r(num, SIGMA1_TAP1) ^ rotate_r(num, SIGMA1_TAP2) ^ rotate_r(num, SIGMA1_TAP3);
}

static inline word_t esigma0(word_t num)
{
	return rotate_r(num, ESIGMA0_TAP1) ^ rotate_r(num, ESIGMA0_TAP2) ^ (num >> ESIGMA0_SHIFT);
}

static inline word_t esigma1(word_t num)
{
	return rotate_r(num, ESIGMA1_TAP1) ^ rotate_r(num, ESIGMA1_TAP2) ^ (num >> ESIGMA1_SHIFT);
}

#define ROUND(A, B, C, D, E, F, G, H, W, K) do { \
	word_t x = H + K + W + sigma1(E) + choose(E, F, G); \
	D = D + x; \
	H = x + sigma0(A) + majority(A, B, C); \
} while(0)

#define WROUND(W0, W1, W2, W3) do { \
	word_t xsigma0 = esigma0(W1); \
	word_t xsigma1 = esigma1(W3); \
	W0 = W0 + xsigma0 + xsigma1 + W2; \
} while(0)

#define ROUND16(A, B, C, D, E, F, G, H, DB, RK) do { \
	ROUND(A, B, C, D, E, F, G, H, DB[0], RK[0]); \
	ROUND(H, A, B, C, D, E, F, G, DB[1], RK[1]); \
	ROUND(G, H, A, B, C, D, E, F, DB[2], RK[2]); \
	ROUND(F, G, H, A, B, C, D, E, DB[3], RK[3]); \
	ROUND(E, F, G, H, A, B, C, D, DB[4], RK[4]); \
	ROUND(D, E, F, G, H, A, B, C, DB[5], RK[5]); \
	ROUND(C, D, E, F, G, H, A, B, DB[6], RK[6]); \
	ROUND(B, C, D, E, F, G, H, A, DB[7], RK[7]); \
	ROUND(A, B, C, D, E, F, G, H, DB[8], RK[8]); \
	ROUND(H, A, B, C, D, E, F, G, DB[9], RK[9]); \
	ROUND(G, H, A, B, C, D, E, F, DB[10], RK[10]); \
	ROUND(F, G, H, A, B, C, D, E, DB[11], RK[11]); \
	ROUND(E, F, G, H, A, B, C, D, DB[12], RK[12]); \
	ROUND(D, E, F, G, H, A, B, C, DB[13], RK[13]); \
	ROUND(C, D, E, F, G, H, A, B, DB[14], RK[14]); \
	ROUND(B, C, D, E, F, G, H, A, DB[15], RK[15]); \
} while(0)

#define ROUND16_2(A, B, C, D, E, F, G, H, DB, RK) do { \
	WROUND(DB[0], DB[1], DB[9], DB[14]); ROUND(A, B, C, D, E, F, G, H, DB[0], RK[0]); \
	WROUND(DB[1], DB[2], DB[10], DB[15]); ROUND(H, A, B, C, D, E, F, G, DB[1], RK[1]); \
	WROUND(DB[2], DB[3], DB[11], DB[0]); ROUND(G, H, A, B, C, D, E, F, DB[2], RK[2]); \
	WROUND(DB[3], DB[4], DB[12], DB[1]); ROUND(F, G, H, A, B, C, D, E, DB[3], RK[3]); \
	WROUND(DB[4], DB[5], DB[13], DB[2]); ROUND(E, F, G, H, A, B, C, D, DB[4], RK[4]); \
	WROUND(DB[5], DB[6], DB[14], DB[3]); ROUND(D, E, F, G, H, A, B, C, DB[5], RK[5]); \
	WROUND(DB[6], DB[7], DB[15], DB[4]); ROUND(C, D, E, F, G, H, A, B, DB[6], RK[6]); \
	WROUND(DB[7], DB[8], DB[0], DB[5]); ROUND(B, C, D, E, F, G, H, A, DB[7], RK[7]); \
	WROUND(DB[8], DB[9], DB[1], DB[6]); ROUND(A, B, C, D, E, F, G, H, DB[8], RK[8]); \
	WROUND(DB[9], DB[10], DB[2], DB[7]); ROUND(H, A, B, C, D, E, F, G, DB[9], RK[9]); \
	WROUND(DB[10], DB[11], DB[3], DB[8]); ROUND(G, H, A, B, C, D, E, F, DB[10], RK[10]); \
	WROUND(DB[11], DB[12], DB[4], DB[9]); ROUND(F, G, H, A, B, C, D, E, DB[11], RK[11]); \
	WROUND(DB[12], DB[13], DB[5], DB[10]); ROUND(E, F, G, H, A, B, C, D, DB[12], RK[12]); \
	WROUND(DB[13], DB[14], DB[6], DB[11]); ROUND(D, E, F, G, H, A, B, C, DB[13], RK[13]); \
	WROUND(DB[14], DB[15], DB[7], DB[12]); ROUND(C, D, E, F, G, H, A, B, DB[14], RK[14]); \
	WROUND(DB[15], DB[0], DB[8], DB[13]); ROUND(B, C, D, E, F, G, H, A, DB[15], RK[15]); \
} while(0)


static void sha2_compress(word_t* chain, word_t* block)
{
	word_t A = chain[0], B = chain[1], C = chain[2], D = chain[3];
	word_t E = chain[4], F = chain[5], G = chain[6], H = chain[7];
	ROUND16(A, B, C, D, E, F, G, H, block, sha2_rk);
	for(unsigned i = 1; i < MAJ_ROUNDS; i++)
		ROUND16_2(A, B, C, D, E, F, G, H, block, (sha2_rk + 16 * i));
	chain[0] += A; chain[1] += B; chain[2] += C; chain[3] += D;
	chain[4] += E; chain[5] += F; chain[6] += G; chain[7] += H;
}

static inline struct sha2_state* unpack_state(void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (struct sha2_state*)((char*)_state + adjust);
}

static inline const struct sha2_state* unpack_state_const(const void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (sizeof(word_t) - ptr % sizeof(word_t)) % sizeof(word_t);
	return (const struct sha2_state*)((char*)_state + adjust);
}

static void sha2_init_low(void* _state, const word_t iv[8], size_t b)
{
	struct sha2_state* state = unpack_state(_state);
	for(unsigned i = 0; i < 8; i++) state->chain[i] = iv[i];
	for(unsigned i = 0; i < 16; i++) state->block[i] = 0;
	state->bits_lo = 0;
	state->bits_hi = 0;
	state->outsize = b;
}

static inline void _sha2_input(struct sha2_state* state, const uint8_t* buf, size_t bufsize)
{
	while(bufsize > 0) {
		if((state->bits_lo & BLOCKBITMASK) == 0 && bufsize >= BLOCKBYTES) {
			//Do entiere block at once.
			XORLOAD(state->block, buf, BLOCKBYTES >> WORDSHIFT);
			bufsize -= BLOCKBYTES;
			buf += BLOCKBYTES;
			state->bits_lo += BLOCKBITS;
		} else if((state->bits_lo & WORDBITMASK) == 0 && bufsize >= WORDBYTES) {
			//Do word at once.
			word_t* ptr = &state->block[(state->bits_lo & BLOCKBITMASK) >> (3 + WORDSHIFT)];
			XORLOAD_1(ptr, buf);
			bufsize -= WORDBYTES;
			buf += WORDBYTES;
			state->bits_lo += WORDBITS;
		} else {
			//Do byte at a time.
			unsigned blockfill = (state->bits_lo & BLOCKBITMASK) >> 3;
			state->block[blockfill >> WORDSHIFT] |= (word_t)buf[0]
				<< ((WORDHBYTE - (blockfill & WORDHBYTE)) << 3);
			bufsize--;
			buf++;
			state->bits_lo += 8;
		}
		if((state->bits_lo & BLOCKBITMASK) == 0) {
			sha2_compress(state->chain, state->block);
			for(unsigned i = 0; i < 16; i++) state->block[i] = 0;
		}
		//Any multiple increment has to be algined, so this can't overflow past zero.
		if(!state->bits_lo) state->bits_hi++;
	}
}

static void sha2_input(void* _state, const uint8_t* buf, size_t bufsize)
{
	struct sha2_state* state = unpack_state(_state);
	_sha2_input(state, buf, bufsize);
}

static void sha2_input_v(void* _state, const struct hashlib_msgf* iov, size_t iovcnt)
{
	struct sha2_state* state = unpack_state(_state);
	for(size_t i = 0; i < iovcnt; i++) {
		_sha2_input(state, (const uint8_t*)iov[i].msgf_base, iov[i].msgf_len);
	}
}

static void sha2_result(const void* _state, uint8_t* out)
{
	const struct sha2_state* state = unpack_state_const(_state);
	unsigned bytes = state->outsize;
	word_t chain[8];
	word_t block[16];
	word_t blockfill;

	for(unsigned i = 0; i < 8; i++) chain[i] = state->chain[i];
	for(unsigned i = 0; i < 16; i++) block[i] = state->block[i];
	blockfill = (state->bits_lo & BLOCKBITMASK) >> 3;

	//Push the marker 0x80.
	block[blockfill >> WORDSHIFT] |= 128ULL << ((WORDHBYTE - (blockfill & WORDHBYTE)) << 3);
	blockfill++;
	if(blockfill > 14 * WORDBYTES) {
		//The length won't fit into first block, so do cycle.
		sha2_compress(chain, block);
		for(unsigned i = 0; i < 16; i++) block[i] = 0;
	}
	//Shifts, since this is in bits.
	block[14] = state->bits_hi;
	block[15] = state->bits_lo;
	sha2_compress(chain, block);
	//Unpack the chain.
	for(unsigned i = 0; i < bytes; i++)
		out[i] = chain[i >> WORDSHIFT] >> ((WORDHBYTE - (i & WORDHBYTE)) << 3);
}

static void sha2_copy(void* dest, const void* src)
{
	const struct sha2_state* _src = unpack_state_const(src);
	struct sha2_state* _dest = unpack_state(dest);
	*_dest = *_src;
}

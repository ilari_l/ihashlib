#include "sha512_ext.h"
#include "sha512.h"
typedef uint64_t word_t;
#include "sha2state.inc"

#define hstride (sizeof(struct sha2_state) + sizeof(uint64_t))
#define SHA512BS 128

static void sha512_ext_init(void* _state, const char** ivblocks, unsigned lanes, unsigned size)
{
	((uint8_t*)_state)[0] = lanes;
	((uint8_t*)_state)[1] = size >> 8;
	((uint8_t*)_state)[2] = size;
	for(unsigned i = 0; i < lanes; i++) {
		sha_512.init((uint8_t*)_state + i * hstride + 8);
		sha_512.update((uint8_t*)_state + i * hstride + 8, (uint8_t*)ivblocks[i], sha_512.block_octets);
	}
}

static inline void sha512e_input(void* _state, const uint8_t* buf, size_t bufsize)
{
	unsigned lanes = ((uint8_t*)_state)[0];
	for(unsigned i = 0; i < lanes; i++)
		sha_512.update((uint8_t*)_state + i * hstride + 8, buf, bufsize);
}

static void sha512e_input_v(void* state, const struct hashlib_msgf* iov, size_t iovcnt)
{
	for(size_t i = 0; i < iovcnt; i++)
		sha512e_input(state, (const uint8_t*)iov[i].msgf_base, iov[i].msgf_len);
}

static void sha512e_result(const void* _state, uint8_t* out)
{
	size_t factor = ((uint8_t*)_state)[0];
	size_t toctets = ((uint8_t*)_state)[1] * 256 + ((uint8_t*)_state)[2];
	size_t octets = toctets / factor;
	uint8_t x[sha_512.output_octets_def];
	for(size_t i = 0; i < factor; i++) {
		sha_512.final((uint8_t*)_state + i * hstride + 8, x);
		for(size_t j = 0; j < octets; j++)
			out[octets * i + j] = x[j];
	}
}

static void sha512e_688_init(void* _state)
{
	const char ivblock1[SHA512BS] = {83,72,65,45,53,49,50,47,54,56,56,112,97,114,116,48,47,50};
	const char ivblock2[SHA512BS] = {83,72,65,45,53,49,50,47,54,56,56,112,97,114,116,49,47,50};
	const char* ivblocks[] = {ivblock1,ivblock2};
	sha512_ext_init(_state,ivblocks,2,86);
}

static void sha512e_794_init(void* _state)
{
	const char ivblock1[SHA512BS] = {83,72,65,45,53,49,50,47,55,57,52,112,97,114,116,48,47,50};
	const char ivblock2[SHA512BS] = {83,72,65,45,53,49,50,47,55,57,52,112,97,114,116,49,47,50};
	const char* ivblocks[] = {ivblock1,ivblock2};
	sha512_ext_init(_state,ivblocks,2,98);
}

static void sha512e_832_init(void* _state)
{
	const char ivblock1[SHA512BS] = {83,72,65,45,53,49,50,47,56,51,50,112,97,114,116,48,47,50};
	const char ivblock2[SHA512BS] = {83,72,65,45,53,49,50,47,56,51,50,112,97,114,116,49,47,50};
	const char* ivblocks[] = {ivblock1,ivblock2};
	sha512_ext_init(_state,ivblocks,2,104);
}

static void sha512e_912_init(void* _state)
{
	const char ivblock1[SHA512BS] = {83,72,65,45,53,49,50,47,57,49,50,112,97,114,116,48,47,50};
	const char ivblock2[SHA512BS] = {83,72,65,45,53,49,50,47,57,49,50,112,97,114,116,49,47,50};
	const char* ivblocks[] = {ivblock1,ivblock2};
	sha512_ext_init(_state,ivblocks,2,114);
}

static void sha512e_1056_init(void* _state)
{
	const char ivblock1[SHA512BS] = {83,72,65,45,53,49,50,47,49,48,53,54,112,97,114,116,48,47,52};
	const char ivblock2[SHA512BS] = {83,72,65,45,53,49,50,47,49,48,53,54,112,97,114,116,49,47,52};
	const char ivblock3[SHA512BS] = {83,72,65,45,53,49,50,47,49,48,53,54,112,97,114,116,50,47,52};
	const char ivblock4[SHA512BS] = {83,72,65,45,53,49,50,47,49,48,53,54,112,97,114,116,51,47,52};
	const char* ivblocks[] = {ivblock1,ivblock2,ivblock3,ivblock4};
	sha512_ext_init(_state,ivblocks,4,132);
}

static void sha512e_copy(void* dest, const void* src)
{
	//Copy the headers.
	unsigned lanes = ((uint8_t*)dest)[0] = ((const uint8_t*)src)[0];
	((uint8_t*)dest)[1] = ((const uint8_t*)src)[1];		//Size high.
	((uint8_t*)dest)[2] = ((const uint8_t*)src)[2];		//Size low.
	for(unsigned i = 0; i < lanes; i++) {
		sha_512.copy((uint8_t*)dest + i * hstride + 8, (const uint8_t*)src + i * hstride + 8);
	}
}

const struct hashlib_hash sha_512_688 = {
	.name = NAME_sha_512_688,
	.state_size = 2 *  hstride + 8,
	.output_octets_min = 86, .output_octets_def = 86, .output_octets_max = 86,
	.block_octets = SHA512BS,
	.init = sha512e_688_init,
	.update = sha512e_input,
	.final = sha512e_result,
	.update_v = sha512e_input_v,
	.copy = sha512e_copy,
};

const struct hashlib_hash sha_512_794 = {
	.name = NAME_sha_512_794,
	.state_size = 2 *  hstride + 8,
	.output_octets_min = 98, .output_octets_def = 98, .output_octets_max = 98,
	.block_octets = SHA512BS,
	.init = sha512e_794_init,
	.update = sha512e_input,
	.final = sha512e_result,
	.update_v = sha512e_input_v,
	.copy = sha512e_copy,
};

const struct hashlib_hash sha_512_832 = {
	.name = NAME_sha_512_832,
	.state_size = 2 *  hstride + 8,
	.output_octets_min = 104, .output_octets_def = 104, .output_octets_max = 104,
	.block_octets = SHA512BS,
	.init = sha512e_832_init,
	.update = sha512e_input,
	.final = sha512e_result,
	.update_v = sha512e_input_v,
	.copy = sha512e_copy,
};

const struct hashlib_hash sha_512_912 = {
	.name = NAME_sha_512_912,
	.state_size = 2 *  hstride + 8,
	.output_octets_min = 114, .output_octets_def = 114, .output_octets_max = 114,
	.block_octets = SHA512BS,
	.init = sha512e_912_init,
	.update = sha512e_input,
	.final = sha512e_result,
	.update_v = sha512e_input_v,
	.copy = sha512e_copy,
};

const struct hashlib_hash sha_512_1056 = {
	.name = NAME_sha_512_1056,
	.state_size = 4 *  hstride + 8,
	.output_octets_min = 132, .output_octets_def = 132, .output_octets_max = 132,
	.block_octets = SHA512BS,
	.init = sha512e_1056_init,
	.update = sha512e_input,
	.final = sha512e_result,
	.update_v = sha512e_input_v,
	.copy = sha512e_copy,
};

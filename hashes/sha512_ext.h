#ifndef _hashlib_sha512ext_h_included_
#define _hashlib_sha512ext_h_included_

#include "ihashlib.h"
#include "libpfx.h"

#define NAME_sha_512_688 "SHA-512/688"
#define NAME_sha_512_794 "SHA-512/794"
#define NAME_sha_512_832 "SHA-512/832"
#define NAME_sha_512_912 "SHA-512/912"
#define NAME_sha_512_1056 "SHA-512/1056"

#define sha_512_688 FAKE_NOEXPORT(sha_512_688)
#define sha_512_794 FAKE_NOEXPORT(sha_512_794)
#define sha_512_832 FAKE_NOEXPORT(sha_512_832)
#define sha_512_912 FAKE_NOEXPORT(sha_512_912)
#define sha_512_1056 FAKE_NOEXPORT(sha_512_1056)

extern const struct hashlib_hash sha_512_688;
extern const struct hashlib_hash sha_512_794;
extern const struct hashlib_hash sha_512_832;
extern const struct hashlib_hash sha_512_912;
extern const struct hashlib_hash sha_512_1056;

#endif

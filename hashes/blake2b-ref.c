#ifdef BLAKE2B_REFERENCE
#include "blake2.h"
#include "blake2b.h"
#include "utils.h"

#define STRUCT_ALIGNMENT 64
#define BLOCKBYTES 128
#define BLOCKBYTEMASK (BLOCKBYTES - 1)
#define WORDSHIFT 3
#define WORDBYTES (1 << WORDSHIFT)
#define WORDBYTEMASK (WORDBYTES - 1)

typedef blake2b_state blake2b_state_t;
typedef uint64_t word_t;

static inline blake2b_state* unpack_state(void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (STRUCT_ALIGNMENT - ptr % STRUCT_ALIGNMENT) % STRUCT_ALIGNMENT;
	return (blake2b_state*)((char*)_state + adjust);
}

static inline const blake2b_state* unpack_state_const(const void* _state)
{
	size_t ptr = (size_t)_state;
	size_t adjust = (STRUCT_ALIGNMENT - ptr % STRUCT_ALIGNMENT) % STRUCT_ALIGNMENT;
	return (const blake2b_state*)((char*)_state + adjust);
}

static int blake2bref_init(void* _state, size_t rbytes)
{
	if(rbytes && rbytes <= 64) {
		blake2b_state* state = unpack_state(_state);
		//Stick the byte count to some interesting place.
		if((size_t)state == (size_t)_state)
			*(uint8_t*)(state + 1) = rbytes;
		else
			*(uint8_t*)_state = rbytes;
		blake2b_init(state, rbytes);
		return 1;
	} else
		return 0;
}

static void blake2bref_224_init(void* _state) { blake2bref_init(_state, 28); }
static void blake2bref_256_init(void* _state) { blake2bref_init(_state, 32); }
static void blake2bref_384_init(void* _state) { blake2bref_init(_state, 48); }
static void blake2bref_512_init(void* _state) { blake2bref_init(_state, 64); }

static inline void _blake2bref_input(blake2b_state* state, const uint8_t* buf, size_t bufsize)
{
	blake2b_update(state, buf, bufsize);
}

static void blake2bref_input(void* _state, const uint8_t* buf, size_t bufsize)
{
	blake2b_state* state = unpack_state(_state);
	_blake2bref_input(state, buf, bufsize);
}

static void blake2bref_input_v(void* _state, const struct hashlib_msgf* iov, size_t iovcnt)
{
	blake2b_state* state = unpack_state(_state);
	for(size_t i = 0; i < iovcnt; i++)
		_blake2bref_input(state, (const uint8_t*)iov[i].msgf_base, iov[i].msgf_len);
}

static void blake2bref_result(const void* _state, uint8_t* out)
{
	const blake2b_state* state = unpack_state_const(_state);
	blake2b_state state2 = *state;
	//Recover the byte count from some interesting place.
	size_t bytes = 0;
	if((size_t)state == (size_t)_state)
		bytes = *(const uint8_t*)(state + 1);
	else
		bytes = *(const uint8_t*)_state;
	blake2b_final(&state2, out, bytes);
}

static void blake2bref_copy(void* dest, const void* src)
{
	const blake2b_state* _src = unpack_state_const(src);
	blake2b_state* _dest = unpack_state(dest);
	*_dest = *_src;
	//Copy the byte counter.
	size_t bytes = 0;
	if((size_t)src == (size_t)_src)
		bytes = *(const uint8_t*)(_src + 1);
	else
		bytes = *(const uint8_t*)src;
	if((size_t)dest == (size_t)_dest)
		*(uint8_t*)(_src + 1) = bytes;
	else
		*(uint8_t*)src = bytes;

}

const struct hashlib_hash blake2b_512 = {
	.name = NAME_blake2b_512,
	.state_size = sizeof(blake2b_state) + STRUCT_ALIGNMENT,
	.output_octets_min = 64, .output_octets_def = 64, .output_octets_max = 64,
	.block_octets = BLOCKBYTES,
	.init = blake2bref_512_init,
	.update = blake2bref_input,
	.final = blake2bref_result,
	.update_v = blake2bref_input_v,
	.copy = blake2bref_copy,
};

const struct hashlib_hash blake2b_384 = {
	.name = NAME_blake2b_384,
	.state_size = sizeof(blake2b_state) + STRUCT_ALIGNMENT,
	.output_octets_min = 48, .output_octets_def = 48, .output_octets_max = 48,
	.block_octets = BLOCKBYTES,
	.init = blake2bref_384_init,
	.update = blake2bref_input,
	.final = blake2bref_result,
	.update_v = blake2bref_input_v,
	.copy = blake2bref_copy,
};

const struct hashlib_hash blake2b_256 = {
	.name = NAME_blake2b_256,
	.state_size = sizeof(blake2b_state) + STRUCT_ALIGNMENT,
	.output_octets_min = 32, .output_octets_def = 32, .output_octets_max = 32,
	.block_octets = BLOCKBYTES,
	.init = blake2bref_256_init,
	.update = blake2bref_input,
	.final = blake2bref_result,
	.update_v = blake2bref_input_v,
	.copy = blake2bref_copy,
};

const struct hashlib_hash blake2b_224 = {
	.name = NAME_blake2b_224,
	.state_size = sizeof(blake2b_state) + STRUCT_ALIGNMENT,
	.output_octets_min = 28, .output_octets_def = 28, .output_octets_max = 68,
	.block_octets = BLOCKBYTES,
	.init = blake2bref_224_init,
	.update = blake2bref_input,
	.final = blake2bref_result,
	.update_v = blake2bref_input_v,
	.copy = blake2bref_copy,
};

#endif
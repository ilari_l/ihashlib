#ifndef _hashlib_utils_h_included_
#define _hashlib_utils_h_included_

#include <stdint.h>
#include <stdlib.h>
#include "libpfx.h"

#define streq FAKE_NOEXPORT(streq)

/**
 * Compare two NUL-terminated strings for equality.
 *
 * \param left The first string.
 * \param right The second string.
 * \returns 1 if strings are equal, 0 if not.
 */
int streq(const char* left, const char* right);

/**
 * Compute XOR of sequence of words with sequence of words as bytes.
 */
static inline void xor_load_be32(uint32_t* w, const uint8_t* b, size_t nw)
{
	for(unsigned n = 0; n < nw; n++)
		for(unsigned i = 0; i < 4; i++) w[n] ^= (uint32_t)b[i + 4 * n] << ((3 - (i&3)) << 3);
}

static inline void xor_load_le32(uint32_t* w, const uint8_t* b, size_t nw)
{
	for(unsigned n = 0; n < nw; n++)
		for(unsigned i = 0; i < 4; i++) w[n] ^= (uint32_t)b[i + 4 * n] << ((i&3) << 3);
}

static inline void xor_load_be64(uint64_t* w, const uint8_t* b, size_t nw)
{
	for(unsigned n = 0; n < nw; n++)
		for(unsigned i = 0; i < 8; i++) w[n] ^= (uint64_t)b[i + 8 * n] << ((7 - (i&7)) << 3);
}

static inline void xor_load_le64(uint64_t* w, const uint8_t* b, size_t nw)
{
	for(unsigned n = 0; n < nw; n++)
		for(unsigned i = 0; i < 8; i++) w[n] ^= (uint64_t)b[i + 8 * n] << ((i&7) << 3);
}

static inline void xor_load_be32_1(uint32_t* w, const uint8_t* b)
{
	for(unsigned i = 0; i < 4; i++) *w ^= (uint32_t)b[i] << ((3 - i) << 3);
}

static inline void xor_load_le32_1(uint32_t* w, const uint8_t* b)
{
	for(unsigned i = 0; i < 4; i++) *w ^= (uint32_t)b[i] << (i << 3);
}

static inline void xor_load_be64_1(uint64_t* w, const uint8_t* b)
{
	for(unsigned i = 0; i < 8; i++) *w ^= (uint64_t)b[i] << ((7 - i) << 3);
}

static inline void xor_load_le64_1(uint64_t* w, const uint8_t* b)
{
	for(unsigned i = 0; i < 8; i++) *w ^= (uint64_t)b[i] << (i << 3);
}

#endif

#ifndef _ihashlib_h_included_
#define _ihashlib_h_included_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

/**
 * A iovector fragment.
 *
 * This is type similar to struct iovec. The reason for not just using struct iovec is that it isn't available on
 * all platforms. If struct iovec exists, this structure has the same representation.
 */
struct hashlib_msgf
{
	const void* msgf_base;
	size_t msgf_len;
};

/**
 * An hash function.
 *
 * This struct represents an hash function.
 */
struct hashlib_hash
{
/**
 * Name of the hash.
 */
	const char* name;
/**
 * Blocksize of the hash in octets (for e.g. computing HMAC)
 */
	size_t block_octets;
/**
 * Minimum output size of the hash in octets.
 */
	size_t output_octets_min;
/**
 * Usual output size of the hash in octets.
 */
	size_t output_octets_def;
/**
 * Maximum output size of the hash in octets.
 */
	size_t output_octets_max;
/**
 * State size of the hash function in bytes.
 */
	size_t state_size;
/**
 * Reset the hash function state.
 *
 * \param state The state (state_size bytes).
 * \note This always uses default hash size.
 * \note If size is outside bounds of output_octets_min/output_octets_max, the behaviour is _undefined_. Error return
 *	is not guaranteed.
 */
	void (*init)(void* state);
/**
 * Reset the hash function state with specific hash size.
 *
 * \param state The state (state_size bytes).
 * \param size The size of hash output in octets.
 * \returns 1 on success, 0 on failure (invalid output size).
 * \note If size is outside bounds of output_octets_min/output_octets_max, the behaviour is _undefined_. Error return
 *	is not guaranteed.
 * \note This is NULL for hashes that don't support variable sizes.
 */
	int (*init_size)(void* state, size_t size);
/**
 * Update the hash function state by appending data.
 *
 * \param state The state (state_size bytes).
 * \param data The data to append.
 * \param datasize Number of octets in data to append. 
 */
	void (*update)(void* state, const uint8_t* data, size_t datasize);
/**
 * Finalize hash function state and return hash result.
 *
 * \param state The state (state_size bytes). Not mutated.
 * \param hashval The hash output. outoctets octets.
 */
	void (*final)(const void* state, uint8_t* hashval);
/**
 * Call sequence of updates.
 *
 * \param state The state (state_size bytes).
 * \param iov The IO vector to process.
 * \param iovcnt The size of IO vector to process.
 * \note If this is NULL, hashlib_update_v() emulates the functionality via ->update().
 */
	void (*update_v)(void* state, const struct hashlib_msgf* iov, size_t iovcnt);
/**
 * Copy hash state to another.
 *
 * \param dest The destination state
 * \param src The source state
 */
	void (*copy)(void* dest, const void* src);
};

/**
 * List all known hashes.
 *
 * \returns The hase list, NULL terminated.
 */
const char** hashlib_hash_list();

/**
 * Look up hash function.
 *
 * \param name The name of hash function, NUL terminated.
 * \returns The handle of hash, or NULL if no such hash is found.
 */
const struct hashlib_hash* hashlib_hash_lookup(const char* name);

/**
 * Return hash->name
 */
const char* hashlib_name(const struct hashlib_hash* hash);
/**
 * Return hash->block_octets
 */
size_t hashlib_block_octets(const struct hashlib_hash* hash);
/**
 * Return hash->output_octets_min
 */
size_t hashlib_output_octets_min(const struct hashlib_hash* hash);
/**
 * Return hash->output_octets_def
 */
size_t hashlib_output_octets_def(const struct hashlib_hash* hash);
/**
 * Return hash->output_octets_max
 */
size_t hashlib_output_octets_max(const struct hashlib_hash* hash);
/**
 * Return hash->state_size
 */
size_t hashlib_state_size(const struct hashlib_hash* hash);
/**
 * Call hash->init
 */
void hashlib_init(const struct hashlib_hash* hash, void* state);
/**
 * Call hash->init_size
 */
int hashlib_init_size(const struct hashlib_hash* hash, void* state, size_t size);
/**
 * Call hash->update
 */
void hashlib_update(const struct hashlib_hash* hash, void* state, const uint8_t* data, size_t datasize);
/**
 * Call hash->final
 */
void hashlib_final(const struct hashlib_hash* hash, const void* state, uint8_t* hashval);
/**
 * Call hash->update_v
 */
void hashlib_update_v(const struct hashlib_hash* hash, void* state, const struct hashlib_msgf* iov, size_t iovcnt);

#ifdef __cplusplus
}
#endif

#endif

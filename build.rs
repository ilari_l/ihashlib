use std::process::Command;
use std::env;

fn compile(module: &str)
{
	let target = env::var("TARGET").unwrap();
	let is_windows = target.contains("-pc-windows");
	let mut cmd = Command::new("gcc");
	cmd.args(&["-g", "-Werror", "-Wall", "-O3", "-std=c99", "-I.", "-c"]);
	if !is_windows {
		cmd.arg("-fPIC");
	}
	cmd.arg("-o");
	cmd.arg(format!("{}.o", module));
	cmd.arg(format!("{}.c", module));
	println!("Compiling {}...", module);
	match cmd.status() {
		Ok(x) => match x.success() {
			true => (),
			false => panic!("GCC failed")
		},
		Err(_) => panic!("Failed to run GCC")
	};
}

fn link(modules: &[&str])
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let mut cmd = Command::new("ld");
	cmd.args(&["-r", "-o", "ihashlib-rust-1.o"]);
	for i in modules.iter() {
		cmd.arg(format!("{}.o", i));
	}
	println!("Partial linking...");
	match cmd.status() {
		Ok(x) => match x.success() {
			true => (),
			false => panic!("LD failed")
		},
		Err(_) => panic!("Failed to run LD")
	};
	let mut cmd = Command::new("ar");
	cmd.arg("cvrs");
	cmd.arg(format!("{}/libihashlib.a", out_dir));
	cmd.arg("ihashlib-rust-1.o");
	println!("Archiving...");
	match cmd.status() {
		Ok(x) => match x.success() {
			true => (),
			false => panic!("AR failed")
		},
		Err(_) => panic!("Failed to run AR")
	};
}

fn main()
{
	let out_dir = env::var("OUT_DIR").unwrap();
	let filebase = [
		"hashes/sha256", "hashes/sha512", "hashes/blake2b", "hashes/sha512_ext", "hashes/skein",
		"hashes/sha3", "hashes/blake2b-ref", "hashlib", "utils"
	];
	for i in filebase.iter() { compile(i) }
	link(&filebase);
	println!("cargo:rustc-link-search=native={}", out_dir);
	println!("cargo:rustc-link-lib=static=ihashlib");
}

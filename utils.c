#include "utils.h"

int streq(const char* left, const char* right)
{
	while(*left && *right) {
		if(*left != *right) return 0;
		left++;
		right++;
	}
	return (*left == *right);
}

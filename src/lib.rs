#![allow(improper_ctypes)]
extern crate libc;
use std::iter::repeat;
use std::slice::from_raw_parts;
use std::str::from_utf8_unchecked;
use std::fmt::{Debug, Formatter, Error as FmtError};
use std::sync::{Once, ONCE_INIT};
use libc::{c_int, c_void, size_t, c_char};
#[cfg(test)]
mod tests;

const MAX_HASHES: usize = 64;
const MAX_HASH_NAME: usize = 64;
static mut HASH_NAME_LIST: [&'static str; MAX_HASHES] = [""; MAX_HASHES];
static mut HASH_NAME_LIST_LEN: usize = 0usize;

///A hash function.
#[repr(C)]
pub struct Hash {}

impl Debug for Hash
{
	fn fmt(&self, f: &mut Formatter) -> Result<(), FmtError>
	{
		write!(f, "<Hash function '{}'>", self.name())
	}
}

///A hash function context.
#[derive(Debug)]
pub struct Context
{
	hash: &'static Hash,
	state: Vec<u8>,
	output: usize
}

impl Clone for Context
{
	fn clone(&self) -> Context
	{
		let mut ret = Context{hash:self.hash, state:repeat(0u8).take(self.hash.state_size()).
			collect(), output:self.output};
		ret.clone_from(self);
		ret
	}
	fn clone_from(&mut self, src: &Context)
	{
		let needed = src.hash.state_size();
		if self.state.len() < needed {
			self.state = repeat(0u8).take(needed).collect();
		}
		self.hash = src.hash;
		self.output = src.output;
		unsafe { hashlib_copy(src.hash._low(), self.state.as_mut_ptr() as *mut c_void,
			src.state.as_ptr() as *const c_void) };
	}
}

#[repr(C)]
struct Fragment
{
	base: *const c_void,
	size: size_t
}

extern
{
	fn hashlib_hash_list() -> *const *const c_char;
	fn hashlib_hash_lookup(name: *const c_char) -> *const Hash;
	fn hashlib_name(hash: *const Hash) -> *const c_char;
	fn hashlib_block_octets(hash: *const Hash) -> size_t;
	fn hashlib_output_octets_min(hash: *const Hash) -> size_t;
	fn hashlib_output_octets_def(hash: *const Hash) -> size_t;
	fn hashlib_output_octets_max(hash: *const Hash) -> size_t;
	fn hashlib_state_size(hash: *const Hash) -> size_t;
	fn hashlib_init(hash: *const Hash, state: *mut c_void);
	fn hashlib_init_size(hash: *const Hash, state: *mut c_void, size: size_t) -> c_int;
	fn hashlib_update(hash: *const Hash, state: *mut c_void, data: *const u8, datasize: size_t);
	fn hashlib_final(hash: *const Hash, state: *const c_void, hashval: *mut u8);
	fn hashlib_update_v(hash: *const Hash, state: *mut c_void, iov: *const Fragment, iovcnt: size_t);
	fn hashlib_copy(hash: *const Hash, dest: *mut c_void, src: *const c_void);
}

unsafe fn c_string_to_rust_string(string: *const c_char) -> &'static str
{
	//Count length.
	let mut len = 0isize;
	while *string.offset(len) != 0 { len += 1; }
	from_utf8_unchecked(from_raw_parts(string as *const u8, len as usize))
}

fn rust_string_to_c_string(_name: &mut [c_char], name: &str) -> Result<(), ()>
{
	//If name is too long, assume invalid.
	let bname = name.as_bytes();
	if bname.len() >= _name.len() { return Err(()); }
	for i in 0..bname.len() {
		if bname[i] == 0  { return Err(()); }	//Embedded NUL not allowed.
		_name[i] = bname[i] as c_char;
	}
	_name[bname.len()] = 0;
	Ok(())
}

impl Hash
{
	///List all available hash function names.
	pub fn list() -> &'static [&'static str]
	{
		static INIT: Once = ONCE_INIT;
		INIT.call_once(|| { unsafe {
			let rlist = hashlib_hash_list();
			//Intern all strings within.
			let mut idx = 0isize;
			while !(*rlist.offset(idx)).is_null() && (idx as usize) < MAX_HASHES {
				HASH_NAME_LIST[idx as usize] = c_string_to_rust_string(*rlist.offset(idx));
				idx += 1;
			}
			HASH_NAME_LIST_LEN = idx as usize;
		}});
		unsafe{&HASH_NAME_LIST[..HASH_NAME_LIST_LEN]}
	}
	///Return object representing the hash function `name`.
	///
	///This method fails if the hash named is not supported.
	pub fn lookup(name: &str) -> Result<&'static Hash, ()>
	{
		let mut _name = [0 as c_char; MAX_HASH_NAME];
		try!(rust_string_to_c_string(&mut _name[..], name));
		unsafe {
			let rscheme = hashlib_hash_lookup(_name.as_ptr());
			if !rscheme.is_null() {
				Ok(&*rscheme)
			} else {
				Err(())
			}	//Not found?
		}
	}
	///Get name of the hash function.
	pub fn name(&self) -> &'static str
	{
		unsafe{c_string_to_rust_string(hashlib_name(self._low()))}
	}
	///Get number of bytes in hash input blocks.
	pub fn block_octets(&self) -> usize
	{
		unsafe{hashlib_block_octets(self._low()) as usize}
	}
	///Get smallest output size supported (in bytes).
	pub fn output_octets_min(&self) -> usize
	{
		unsafe{hashlib_output_octets_min(self._low()) as usize}
	}
	///Get default output size (in bytes).
	pub fn output_octets_def(&self) -> usize
	{
		unsafe{hashlib_output_octets_def(self._low()) as usize}
	}
	///Get largest output size supported (in bytes).
	pub fn output_octets_max(&self) -> usize
	{
		unsafe{hashlib_output_octets_max(self._low()) as usize}
	}
	///Create context with default output size.
	pub fn make(&'static self) -> Context
	{
		let statesize = self.state_size();
		let size = self.output_octets_def();
		let mut ret = Context{hash:self, state:repeat(0u8).take(statesize).collect(), output:size};
		unsafe{hashlib_init(self._low(), ret.state.as_mut_ptr() as *mut c_void)};
		ret
	}
	///Create context with specified output size.
	pub fn make_size(&'static self, size: usize) -> Result<Context, ()>
	{
		let statesize = self.state_size();
		let minsize = self.output_octets_min();
		let maxsize = self.output_octets_max();
		if size < minsize || size > maxsize { return Err(()); }
		let mut ret = Context{hash:self, state:repeat(0u8).take(statesize).collect(), output:size};
		let ok = unsafe{hashlib_init_size(self._low(), ret.state.as_mut_ptr() as *mut c_void,
			size as size_t)};
		if ok <= 0 { return Err(()); }
		Ok(ret)
	}
	fn state_size(&self) -> usize
	{
		unsafe{hashlib_state_size(self._low()) as usize}
	}
	fn _low(&self) -> *const Hash
	{
		self as *const Hash
	}
}

impl Context
{
	//Reset the context, with default output size.
	pub fn reset(&mut self)
	{
		let size = self.hash.output_octets_def();
		unsafe{hashlib_init(self.hash._low(), self.state.as_mut_ptr() as *mut c_void);}
		self.output = size
	}
	//Reset the context, with specified output size. Fails if specified size is invalid.
	pub fn reset_size(&mut self, size: usize) -> Result<(), ()>
	{
		let minsize = self.hash.output_octets_min();
		let maxsize = self.hash.output_octets_max();
		if size < minsize || size > maxsize { return Err(()); }
		let ok = unsafe{hashlib_init_size(self.hash._low(), self.state.as_mut_ptr() as *mut c_void,
			size as size_t)};
		if ok <= 0  { return Err(()); }
		self.output = size;
		Ok(())
	}
	///Update the hash state by appending specified data.
	pub fn update(&mut self, data: &[u8])
	{
		unsafe{hashlib_update(self.hash._low(), self.state.as_mut_ptr() as *mut c_void,
			data.as_ptr(), data.len() as size_t)};
	}
	///Get hash of current data. Fails if buffer is not of correct size.
	pub fn finalize(&self, output: &mut [u8]) -> Result<(), ()>
	{
		if output.len() != self.output { return Err(()); }
		unsafe{hashlib_final(self.hash._low(), self.state.as_ptr() as *const c_void,
			output.as_mut_ptr())};
		Ok(())
	}
	///Update the hash state by appending concatenation of specified data.
	pub fn update_v(&mut self, data: &[&[u8]])
	{
		unsafe{hashlib_update_v(self.hash._low(), self.state.as_mut_ptr() as *mut c_void,
			data.as_ptr() as *const Fragment, data.len() as size_t)};
	}
}

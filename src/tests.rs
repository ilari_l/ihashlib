use super::{Hash};

#[test]
fn print_avail_schemes()
{
	let list = Hash::list();
	assert_eq!(list.iter().count(), 40);
	for i in list.iter() {
		Hash::lookup(i).unwrap();
	}
}

#[test]
fn lookup_nonexistent()
{
	Hash::lookup("DOES-NOT-EXIST").unwrap_err();
}

#[test]
fn lookup_test()
{
	assert_eq!(Hash::lookup("SHA-256").unwrap().name(), "SHA-256");
}

#[test]
fn sha256_params()
{
	assert_eq!(Hash::lookup("SHA-256").unwrap().block_octets(), 64);
	assert_eq!(Hash::lookup("SHA-256").unwrap().output_octets_min(), 32);
	assert_eq!(Hash::lookup("SHA-256").unwrap().output_octets_def(), 32);
	assert_eq!(Hash::lookup("SHA-256").unwrap().output_octets_max(), 32);
}

#[test]
fn shake256_params()
{
	assert_eq!(Hash::lookup("Shake256").unwrap().block_octets(), 136);
	assert_eq!(Hash::lookup("Shake256").unwrap().output_octets_min(), 24);
	assert_eq!(Hash::lookup("Shake256").unwrap().output_octets_def(), 64);
	assert_eq!(Hash::lookup("Shake256").unwrap().output_octets_max(), 2147483647);
}

#[test]
fn sha256_no_size()
{
	let sha256 = Hash::lookup("SHA-256").unwrap();
	sha256.make_size(32).unwrap_err()
}

#[test]
fn shake256_has_size()
{
	let mut buf = [0u8; 32];
	let shake256 = Hash::lookup("Shake256").unwrap();
	let ctx = shake256.make_size(32).unwrap();
	ctx.finalize(&mut buf[..]).unwrap();
}

#[test]
fn shake256_wrong_size()
{
	let mut buf = [0u8; 33];
	let shake256 = Hash::lookup("Shake256").unwrap();
	let ctx = shake256.make_size(32).unwrap();
	ctx.finalize(&mut buf[..]).unwrap_err();
}

#[test]
fn shake256_size_too_small()
{
	let shake256 = Hash::lookup("Shake256").unwrap();
	shake256.make_size(23).unwrap_err()
}


#[test]
fn sha256_abc_hash()
{
	let mut buf = [0u8; 32];
	let sha256 = Hash::lookup("SHA-256").unwrap();
	let mut ctx = sha256.make();
	ctx.update(b"abc");
	ctx.finalize(&mut buf[..]).unwrap();
	assert_eq!(&buf[..], [
		0xba, 0x78, 0x16, 0xbf, 0x8f, 0x01, 0xcf, 0xea, 0x41, 0x41, 0x40, 0xde, 0x5d, 0xae, 0x22, 0x23,
		0xb0, 0x03, 0x61, 0xa3, 0x96, 0x17, 0x7a, 0x9c, 0xb4, 0x10, 0xff, 0x61, 0xf2, 0x00, 0x15, 0xad
	]);
	buf[0] = 0;
	ctx.reset();
	ctx.update(b"abc");
	ctx.finalize(&mut buf[..]).unwrap();
	assert_eq!(&buf[..], [
		0xba, 0x78, 0x16, 0xbf, 0x8f, 0x01, 0xcf, 0xea, 0x41, 0x41, 0x40, 0xde, 0x5d, 0xae, 0x22, 0x23,
		0xb0, 0x03, 0x61, 0xa3, 0x96, 0x17, 0x7a, 0x9c, 0xb4, 0x10, 0xff, 0x61, 0xf2, 0x00, 0x15, 0xad
	]);
}

#[test]
fn sha256_abc_hash_frag()
{
	let mut buf = [0u8; 32];
	let sha256 = Hash::lookup("SHA-256").unwrap();
	let mut ctx = sha256.make();
	ctx.update_v(&[&[97u8][..], &[98u8][..], &[99u8][..]]);
	ctx.finalize(&mut buf[..]).unwrap();
	assert_eq!(&buf[..], [
		0xba, 0x78, 0x16, 0xbf, 0x8f, 0x01, 0xcf, 0xea, 0x41, 0x41, 0x40, 0xde, 0x5d, 0xae, 0x22, 0x23,
		0xb0, 0x03, 0x61, 0xa3, 0x96, 0x17, 0x7a, 0x9c, 0xb4, 0x10, 0xff, 0x61, 0xf2, 0x00, 0x15, 0xad
	]);
}

#[test]
fn sha256_clone_hash()
{
	let mut buf = [0u8; 32];
	let sha256 = Hash::lookup("SHA-256").unwrap();
	let mut ctx = sha256.make();
	ctx.update(b"abc");
	let ctx2 = ctx.clone();
	ctx.reset();
	ctx2.finalize(&mut buf[..]).unwrap();
	assert_eq!(&buf[..], [
		0xba, 0x78, 0x16, 0xbf, 0x8f, 0x01, 0xcf, 0xea, 0x41, 0x41, 0x40, 0xde, 0x5d, 0xae, 0x22, 0x23,
		0xb0, 0x03, 0x61, 0xa3, 0x96, 0x17, 0x7a, 0x9c, 0xb4, 0x10, 0xff, 0x61, 0xf2, 0x00, 0x15, 0xad
	]);
}

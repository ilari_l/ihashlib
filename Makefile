include hashes/Makerules

HASH_FILES=$(patsubst %,hashes/%.X,$(HASH_FILES_RAW))
COMMON_FILES=hashlib.X utils.X

OBJECTS=$(patsubst %.X,%.s.o,$(COMMON_FILES)) $(patsubst %.X,%.s.o,$(HASH_FILES)) 

OBJECTS_D=$(patsubst %.X,%.d.o,$(COMMON_FILES)) $(patsubst %.X,%.d.o,$(HASH_FILES)) 

OBJECTS_P=$(patsubst %.X,%.o,$(COMMON_FILES)) $(patsubst %.X,%.o,$(HASH_FILES)) 

CFLAGS=
LDEXTRA=
PREFIX=/usr/local
MAJORVERSION=1
MINORVERSION=0
PATCH=0
STATICLIB=libihashlib.a
STATICLIB_D=libihashlib_s.a
DYNAMICLIB_BASE=libihashlib.so
DYNAMICLIB=$(DYNAMICLIB_BASE).$(MAJORVERSION).$(MINORVERSION).$(PATCH)
DYNAMICLIB_SONAME=$(DYNAMICLIB_BASE).$(MAJORVERSION)
HEADERFILE=ihashlib.h
PCFILE=ihashlib.pc

all: $(DYNAMICLIB) $(STATICLIB) $(STATICLIB_D) hashlib.test hashlib.test-opt hashlib.speed

install: $(DYNAMICLIB) $(STATICLIB) $(STATICLIB_D) $(HEADERFILE) $(PCFILE)
	install -D -t $(PREFIX)/lib $(DYNAMICLIB) $(STATICLIB) $(STATICLIB_D)
	install -D -t $(PREFIX)/lib/pkgconfig $(PCFILE)
	install -D -t $(PREFIX)/include $(HEADERFILE)
	ln -sf $(PREFIX)/lib/$(DYNAMICLIB) $(PREFIX)/lib/$(DYNAMICLIB_SONAME).$(MINORVERSION)
	ln -sf $(PREFIX)/lib/$(DYNAMICLIB) $(PREFIX)/lib/$(DYNAMICLIB_SONAME)
	ln -sf $(PREFIX)/lib/$(DYNAMICLIB) $(PREFIX)/lib/$(DYNAMICLIB_BASE)

$(PCFILE): $(PCFILE).in
	sed -r -e "s!%%PREFIX%%!$(PREFIX)!" <$^ >$@

$(DYNAMICLIB): hashlib-2.d.o
	gcc -fPIC -shared -Wl,-soname,$(DYNAMICLIB_SONAME) -o $@ $^  $(LDEXTRA)

$(STATICLIB): hashlib-2.s.o
	ar cvrs $@ $^

$(STATICLIB_D): hashlib-2.d.o
	ar cvrs $@ $^

hashlib.test: $(OBJECTS_P) test.o 
	g++ -fprofile-arcs -ftest-coverage -o $@ $^  $(LDEXTRA)

hashlib.test-opt: $(OBJECTS) test-o.o  $(LDEXTRA)
	g++ -o $@ $^

hashlib.speed: $(OBJECTS_D) speedtest.o  $(LDEXTRA)
	g++ -o $@ $^

hashlib-2.d.o: hashlib-1.d.o
	objcopy -w --localize-symbol=!hashlib_* --localize-symbol=* $^ $@

hashlib-2.s.o: hashlib-1.s.o
	objcopy -w --localize-symbol=!hashlib_* --localize-symbol=* $^ $@

hashlib-1.s.o: $(OBJECTS)
	ld -r -o $@ $^  $(LDEXTRA)

hashlib-1.d.o: $(OBJECTS_D)
	ld -r -o $@ $^  $(LDEXTRA)

%.d.o: %.c
	gcc -g -Werror -Wall -O3 -std=c99 -fPIC -I. $(CFLAGS) -c -o $@ $<

%.s.o: %.c
	gcc -g -Werror -Wall -O3 -fno-tree-loop-distribute-patterns -std=c99 -I. $(CFLAGS) -c -o $@ $<

%.o: %.c
	gcc -g -Werror -Wall -fprofile-arcs -ftest-coverage -std=c99 -I. $(CFLAGS) -c -o $@ $<

test.o: test.cpp
	g++ -g -Werror -Wall -fprofile-arcs -ftest-coverage -std=c++11 -DTEST_ECCLIB_CODE -I. -c -o $@ $<

test-o.o: test.cpp
	g++ -g -Werror -Wall -O3 -std=c++11 -I. -DTEST_ECCLIB_CODE -c -o $@ $<

speedtest.o: speedtest.cpp
	g++ -g -Werror -Wall -O3 -std=c++11 -I.  -c -o $@ $<
